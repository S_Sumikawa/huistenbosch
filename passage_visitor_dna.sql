-- version_202110
/*分析 行動DNA算出*/
-- ★★★変数設定★★★
  -- 変数はなし

-- その他変更点(#numberでコード内検索)
  -- #0 出力するテーブル名
  -- #1 使用する来訪日テーブル名
-- ★★★設定終了★★★

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20211108_passage_visitor_dna` AS /*要変更点#0*/

WITH t0 AS(
  SELECT
      id,
      CASE
        WHEN jst_date <= '2021-08-31' THEN 'before'
        WHEN jst_date >= '2021-09-11' THEN 'after'
      END AS period_flag,
      COUNT(DISTINCT jst_DATE) AS cnt_date,
  FROM 
    `beaconbank-analytics.huistenbosch.20211108_passage_visitor`
  WHERE 
    jst_date NOT BETWEEN '2021-09-01' AND '2021-09-10'
    AND jst_hour BETWEEN 9 AND 20
  GROUP BY 1,2
)
, log AS(
  SELECT DISTINCT
    t1.id,
    period_flag,
    cnt_date,
    t2.* except(id,id_type)
   FROM
    (
      SELECT 
        id, period_flag, cnt_date 
      FROM 
        t0 
      WHERE 
            (period_flag = 'before' AND cnt_date < 51)
        OR  (period_flag = 'after' AND cnt_date < 18)
    ) t1 /*要変更点#1*/
    JOIN `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t2 USING (id)
)

/* 横持ちを縦持ちに変換 */
, unpivot AS(
SELECT
 *
FROM
 log
 UNPIVOT (
  SCORE
  FOR genre_Lv3_en IN (
    ETM_ACM_BusinessHotel,
    ETM_ACM_Hotel,
    ETM_ACM_JapaneseHotel,
    -- ETM_ACM_LeisureHotel,
    ETM_ACM_Minshuku,
    ETM_ACM_OtherAccomodation,
    ETM_ACM_Pension,
    ETM_ACM_PublicHotel,
    ETM_AMS_Arcade,
    ETM_AMS_InternetComicCafe,
    ETM_AMS_Karaoke,
    ETM_AMS_Pachinko,
    ETM_AMS_Themepark,
    ETM_APR_Aquarium,
    ETM_APR_Archive,
    ETM_APR_ArtMuseum,
    ETM_APR_Hall,
    ETM_APR_LiveHouse,
    ETM_APR_Museum,
    ETM_APR_OtherAppriciation,
    ETM_APR_Theater,
    ETM_APR_ZooBotanical,
    ETM_OTR_OtherEntertainment,
    ETM_PRK_Park,
    ETM_SPA_Spa,
    ETM_SPO_Ballpark,
    ETM_SPO_Budo,
    ETM_SPO_CyclingRoad,
    ETM_SPO_FitnessGym,
    ETM_SPO_Golf,
    ETM_SPO_OtherSports,
    ETM_SPO_Pool,
    ETM_SPO_SnowMountain,
    ETM_SPO_Stadium,
    ETM_SPO_Tennis,
    ETM_SS_SightSpot,
    GRM_CFS_Cafe,
    GRM_CFS_JapaneseSweets,
    GRM_CFS_OtherSweets,
    GRM_CFS_WesternSweets,
    GRM_FMR_Chinese,
    GRM_FMR_Japanese,
    GRM_FMR_Sushi,
    GRM_FMR_Variety,
    GRM_FMR_Western,
    GRM_FMR_Yakiniku,
    GRM_FST_Bakery,
    GRM_FST_Curry,
    GRM_FST_DonTeishoku,
    GRM_FST_HumbergerShop,
    GRM_FST_OtherFastFood,
    GRM_FST_Ramen,
    GRM_FST_Takeout,
    GRM_FST_UdonSoba,
    GRM_OTR_Asian,
    GRM_OTR_Bar,
    GRM_OTR_Chinese,
    GRM_OTR_Ethnic,
    GRM_OTR_French,
    GRM_OTR_Italian,
    GRM_OTR_Izakaya,
    GRM_OTR_NabeOden,
    GRM_OTR_NigiriSushi,
    GRM_OTR_Okonomiyaki,
    GRM_OTR_OtherRestaurant,
    GRM_OTR_PubClub,
    GRM_OTR_Steak,
    GRM_OTR_Tonkatsu,
    GRM_OTR_TraditionalJapanese,
    GRM_OTR_Unagi,
    GRM_OTR_Yakiniku,
    GRM_OTR_Yakitori,
    LFS_EDU_ElementarySchool,
    LFS_EDU_HighSchool,
    LFS_EDU_Hndicapped,
    LFS_EDU_JuniorCollege,
    LFS_EDU_JuniorHighSchool,
    LFS_EDU_KindergartenNursery,
    LFS_EDU_PrepSchool,
    LFS_EDU_TechnicalCollege,
    LFS_EDU_Univercity,
    LFS_EDU_VocationalSchool,
    -- LFS_FIN_Bank,
    -- LFS_FIN_PostOffice,
    -- LFS_HSP_Clinics,
    -- LFS_HSP_GeneralHospitals,
    LFS_OTR_BeautySalons,
    LFS_OTR_Laundry,
    LFS_OTR_Massage,
    LFS_OTR_PetService,
    -- LFS_PUB_Courtyard,
    -- LFS_PUB_DriversLicenseExam,
    -- LFS_PUB_Embassies,
    -- LFS_PUB_FireFighting,
    -- LFS_PUB_FuneralWedding,
    -- LFS_PUB_GovernmentOffice,
    -- LFS_PUB_Library,
    -- LFS_PUB_MunicipalOffice,
    -- LFS_PUB_OtherPublicService,
    -- LFS_PUB_Police,
    -- LFS_SCL_AbacusShool,
    LFS_SCL_BalletShool,
    LFS_SCL_BusinessSchool,
    LFS_SCL_CalligraphyShool,
    LFS_SCL_CeramicArtShool,
    LFS_SCL_ComputerShool,
    LFS_SCL_CookingShool,
    LFS_SCL_DanceShool,
    LFS_SCL_DressingShool,
    -- LFS_SCL_DrivingSchool,
    LFS_SCL_English,
    LFS_SCL_FlowerArrangementShool,
    LFS_SCL_ForeignLang,
    LFS_SCL_GlassCraftShool,
    LFS_SCL_KnittingShool,
    LFS_SCL_MusicShool,
    LFS_SCL_PaintingArtShool,
    LFS_SCL_PianoShool,
    LFS_SCL_TeaCeremonyShool,
    LFS_TRS_Airport,
    LFS_TRS_BusTerminal,
    -- LFS_TRS_CarCareService,
    LFS_TRS_Carpool,
    -- LFS_TRS_FerryTerminal,
    LFS_TRS_GasStation,
    LFS_TRS_RentaCar,
    LFS_TRS_RoadsideRest,
    LFS_TRS_SAPA,
    LFS_TRS_Station,
    SPG_APL_Accessory,
    SPG_APL_BabyChildren,
    SPG_APL_Bag,
    SPG_APL_FastFashion,
    SPG_APL_FormalWear,
    SPG_APL_Jewelry,
    SPG_APL_Kimono,
    SPG_APL_Lingerie,
    SPG_APL_SecondHand,
    SPG_APL_ShoeStore,
    SPG_APL_Watch,
    SPG_BIG_DepartmentStore,
    SPG_BIG_FurnitureStore,
    SPG_BIG_HomeApplianceStore,
    SPG_BIG_HomeCenter,
    SPG_BIG_MarketStreet,
    SPG_BIG_MiscellaneousGoodsStore,
    SPG_BIG_OtherSC,
    SPG_BIG_OutletMall,
    SPG_BIG_RecycleShop,
    SPG_BIG_ShoppingMallComplex,
    SPG_SML_ConvenienceStore,
    SPG_SML_DiscountStore,
    SPG_SML_DrugStore,
    SPG_SML_FoodMarket,
    SPG_SML_MiniMarket,
    SPG_SML_OneDollerMarket,
    SPG_SPC_BookStore,
    SPG_SPC_CarDealer,
    SPG_SPC_CarSupplyStore,
    SPG_SPC_GolfStore,
    SPG_SPC_HousingExpo,
    SPG_SPC_LiquorStore,
    SPG_SPC_MobileStore,
    SPG_SPC_OtherSpecialityStore,
    SPG_SPC_OutdoorGoodsStore,
    SPG_SPC_PreOwnedCarDealer,
    SPG_SPC_RentVideoCD,
    SPG_SPC_SportsGoodsStore,
    SPG_SPC_Toy
    )
  )
)
/* DNA日本語変換 & gender付与 & 重複排除 */
, join_master AS(
  SELECT DISTINCT
    id,
    period_flag,
    Cat_Lv1_jp, 
    Cat_Lv2_jp, 
    Cat_Lv3_jp,
    score,
    -- CASE
    --   WHEN t3.Age = 10 THEN '10代'
    --   WHEN t3.Age = 20 THEN '20代'
    --   WHEN t3.Age = 30 THEN '30代'
    --   WHEN t3.Age = 40 THEN '40代'
    --   WHEN t3.Age = 50 THEN '50代'
    --   WHEN t3.Age = 60 THEN '60代'
    --   ELSE NULL
    -- END age,
    CASE
      WHEN t4.Gender = 'M' THEN '男性'
      WHEN t4.Gender = 'F' THEN '女性'
      ELSE NULL
    END gender,
  FROM
    unpivot t1
    JOIN `labs-analytics-298709.works_sumikawa.mst_DNA_jp_en_v2` t2 USING(genre_Lv3_en)
    -- LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_age_fac` t3 ON t1.hash_id = TO_HEX(SHA256(t3.id))
    LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_gender` t4 USING(id)
  WHERE
　  score IS NOT NULL
)
, dna_male AS( /*男女別の行動DNA*/
  SELECT
    period_flag,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    SUM(score)/COUNT(DISTINCT id) AS male_ave_score
  FROM
    join_master
  WHERE
    gender = '男性'
  GROUP BY 
    1,2,3,4,5
)
, dna_female AS( /*男女別の行動DNA*/
  SELECT
    period_flag,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    SUM(score)/COUNT(DISTINCT id) AS female_ave_score
  FROM
    join_master
  WHERE
    gender = '女性'
  GROUP BY 
    1,2,3,4,5
)
, dna_total AS( /*行動DNA*/
  SELECT
    period_flag,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score_total/uu_total AS ave_score
  FROM
    ( /*男女のscoreとuuを足し合わせる*/
      SELECT
        period_flag,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp,
        SUM(score) AS sum_score_total,
        COUNT(DISTINCT id) AS uu_total
      FROM
        join_master
      GROUP BY
        period_flag,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp
    )
)
SELECT
  dna_total.*,
  male_ave_score,
  female_ave_score
FROM
  dna_total
  LEFT JOIN dna_male USING(period_flag,Cat_Lv3_jp)
  LEFT JOIN dna_female USING(period_flag,Cat_Lv3_jp)