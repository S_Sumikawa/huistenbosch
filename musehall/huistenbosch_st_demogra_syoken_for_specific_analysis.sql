-- 特定施設来訪者のフラグをつけた st_demogra_syoken テーブルを作成するクエリ

DECLARE date_setting, start_date, end_date DATE;
DECLARE target_polygon STRING;
-- DECLARE repeat_period,min_log_cnt INT64;

-- SET date_setting = CURRENT_DATE('Asia/Tokyo');
-- SET start_date = DATE_TRUNC(DATE_SUB(date_setting, INTERVAL 1 MONTH), MONTH);
-- SET end_date = DATE_SUB(DATE_TRUNC(date_setting, MONTH), INTERVAL 1 DAY);

SET start_date = "2019-07-01";
SET end_date = "2022-04-30";

-- MUSE HALL来訪者
SET target_polygon = '129.79014037701867 33.08519521907036,129.79015647027276 33.08498621851759,129.79053734395288 33.08501543367849,129.7905212506988 33.085217692218485,129.79014037701867 33.08519521907036'
;
-- INSERT INTO 
--     `beaconbank-analytics.pr_db_huistenbosch.st_demogra_syoken`
CREATE OR REPLACE TABLE
    `beaconbank-analytics.pr_db_huistenbosch.st_demogra_syoken_for_specific_analysis` AS

WITH st_demogra_syoken AS (
    SELECT DISTINCT
        *
    FROM 
        `beaconbank-analytics.pr_db_huistenbosch.st_demogra_syoken_dev`
    WHERE store_name = 'アトラクションタウン'
)

, union_log AS (
/*ログテーブル_コロナ期間前*/
    SELECT
        TO_HEX(SHA256(IF(adid IS NULL, CAST(app_user_id AS STRING),adid))) AS id,
        app_user_id,
        date_time,
        CASE
            WHEN EXTRACT(HOUR FROM date_time) BETWEEN  9 AND 10 THEN '09-10時台'
            WHEN EXTRACT(HOUR FROM date_time) BETWEEN 11 AND 13 THEN '11-13時台'
            WHEN EXTRACT(HOUR FROM date_time) BETWEEN 14 AND 16 THEN '14-16時台'
            WHEN EXTRACT(HOUR FROM date_time) BETWEEN 17 AND 19 THEN '17-19時台'
            WHEN EXTRACT(HOUR FROM date_time) BETWEEN 20 AND 21 THEN '20-21時台'
        END AS time_period,
        longitude,
        latitude,
        'before' AS covid_flag
    FROM
        `beaconbank-analytics.analytics_log.gps_*`
    WHERE
        _TABLE_SUFFIX IN('bb_location','vs')
        -- AND DATE(date_time) BETWEEN start_date1 AND end_date1
        AND DATE(date_time) BETWEEN start_date AND end_date
        AND geohash IN('wvuk1qy')
        AND TO_HEX(SHA256(IF(adid IS NULL, CAST(app_user_id AS STRING),adid))) IN (SELECT DISTINCT id FROM st_demogra_syoken)
)

/*集計結果*/
, result AS (
    SELECT DISTINCT
        id,
        DATE(date_time) AS jst_date
    FROM
        union_log
    WHERE
        ST_COVERS(ST_GEOGFROMTEXT(CONCAT("POLYGON((",target_polygon,"))") ),ST_GEOGPOINT(longitude, latitude))
        AND time_period IS NOT NULL
)
SELECT DISTINCT
    t1.*,
    IF(result.id IS NOT NULL,"muse_hall",NULL) AS specific_flag
FROM
    `beaconbank-analytics.pr_db_huistenbosch.st_demogra_syoken_dev`  t1
    LEFT JOIN result USING(id,jst_date) 
;