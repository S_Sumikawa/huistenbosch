WITH musehall AS(
SELECT CONCAT(EXTRACT(YEAR FROM jst_date),"-",EXTRACT(MONTH FROM jst_date)) AS jst_ym,COUNT(DISTINCT id) AS uu, COUNT(id) AS duu,  
FROM `beaconbank-analytics.pr_db_huistenbosch.st_demogra_syoken_for_specific_analysis`
WHERE specific_flag IS NOT NULL
GROUP BY 1
)

, all_park AS(
  SELECT CONCAT(EXTRACT(YEAR FROM jst_date),"-",EXTRACT(MONTH FROM jst_date)) AS jst_ym,COUNT(DISTINCT id) AS uu, COUNT(id) AS duu,  
  FROM `beaconbank-analytics.pr_db_huistenbosch.st_demogra_syoken_for_specific_analysis`
  -- WHERE specific_flag IS NOT NULL
  GROUP BY 1
)
SELECT
  jst_ym,
  all_park.uu,
  all_park.duu,
  musehall.uu AS muse_uu,
  musehall.duu AS muse_duu,
FROM
  all_park
  LEFT JOIN musehall USING(jst_ym)
ORDER BY 1 desc