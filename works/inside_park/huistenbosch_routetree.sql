CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210701_huistenbosch_routetree` AS

SELECT DISTINCT
    *,
    
FROM
    `beaconbank-analytics.huistenbosch.20210701_huistenbosch_beacon_visitor`
WHERE
    visit_per_month <= 15
