CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210701_huistenbosch_beaconlog_profile` AS
SELECT
    adid AS id,
    EXTRACT(DATE FROM TIMESTAMP_ADD(TIMESTAMP(t0.detected_time), INTERVAL 9 HOUR)) AS jst_date, -- DATETIME(detected_time, 'Asia/Tokyo')でおそらく直る
    EXTRACT(HOUR FROM TIMESTAMP_ADD(TIMESTAMP(t0.detected_time), INTERVAL 9 HOUR)) AS hour,
    flag,
    visit_flag,
    repeat_flag,
    cnt_date,
    visit_per_month,
    home_flag,
    night_before,
    night_after,
    recent_visit,
    next_visit,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    'beacon' AS data_from,
    beacon_id,
    t0.uuid,
    t0.major,
    t0.minor,
    area,
    place_name

FROM
    `beaconbank-analytics.huistenbosch.huistenbosch_beaconlog` t0
    LEFT JOIN `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon` t1 USING(minor)
    LEFT JOIN ( 
        SELECT
            DISTINCT id,
            jst_date,
            flag,
            visit_flag,
            repeat_flag,
            cnt_date,
            visit_per_month,
            home_flag,
            night_before,
            night_after,
            recent_visit,
            next_visit,
            Gender,
            Age,
            home_lat,
            home_lon,
            home_pref,
            home_city,
            office_pref,
            office_city
        FROM 
            `beaconbank-analytics.huistenbosch.20210629_huistenbosch_behavior_inside_park` 
        ) t2 
    ON EXTRACT(DATE FROM TIMESTAMP_ADD(TIMESTAMP(t0.detected_time), INTERVAL 9 HOUR)) = t2.jst_date
    AND t0.adid = t2.id