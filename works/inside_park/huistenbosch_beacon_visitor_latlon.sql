CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210706_huistenbosch_beacon_visitor_latlon` AS

SELECT DISTINCT
    id	,
    jst_time	,
    jst_date	,
    hour	,
    flag	,
    visit_flag	,
    repeat_flag	,
    cnt_date	,
    visit_per_month	,
    home_flag	,
    night_before	,
    night_after	,
    recent_visit	,
    next_visit	,
    CASE
      WHEN t0.Gender = 'M' THEN '男性'
      WHEN t0.Gender = 'F' THEN '女性'
      ELSE NULL
    END gender,
    CASE
      WHEN t0.Age = 10 THEN '10代'
      WHEN t0.Age = 20 THEN '20代'
      WHEN t0.Age = 30 THEN '30代'
      WHEN t0.Age = 40 THEN '40代'
      WHEN t0.Age = 50 THEN '50代'
      WHEN t0.Age = 60 THEN '60代'
      ELSE NULL
    END age,
    home_lat	,
    home_lon	,
    home_pref	,
    home_city	,
    office_pref	,
    office_city	,
    data_from	,
    beacon_id	,
    uuid	,
    major	,
    minor	,
    area	,
    CASE
        WHEN place_name = 'カナルクルーザー' AND area = 'タワーシティ' THEN 'カナルクルーザー(タワーシティ)'
        WHEN place_name = 'カナルクルーザー' AND area = 'ウェルカムエリア' THEN 'カナルクルーザー(ウェルカムエリア)'
        ELSE place_name
    END AS place_name,
    NULL AS buffer,
    CONCAT(id, CAST(jst_date AS STRING)) AS id_jst_date,
    latitude,
    longitude,
    latitude AS beacon_lat,
    longetude AS beacon_lon,
FROM
    `beaconbank-analytics.huistenbosch.20210706_huistenbosch_beaconlog_profile_v4` t0

UNION ALL

SELECT DISTINCT
    id,
    TIMESTAMP(jst_time),
    jst_date,
    hour,
    flag,
    visit_flag,
    repeat_flag,
    cnt_date,
    visit_per_month,
    home_flag,
    night_after,
    night_after,
    recent_visit,
    next_visit,
    CASE
      WHEN t0.Gender = 'M' THEN '男性'
      WHEN t0.Gender = 'F' THEN '女性'
      ELSE NULL
    END gender,
    CASE
      WHEN t0.Age = 10 THEN '10代'
      WHEN t0.Age = 20 THEN '20代'
      WHEN t0.Age = 30 THEN '30代'
      WHEN t0.Age = 40 THEN '40代'
      WHEN t0.Age = 50 THEN '50代'
      WHEN t0.Age = 60 THEN '60代'
      ELSE NULL
    END age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    'gps' AS data_from,
    NULL AS beacon_id,
    uuid,
    major,
    minor,
    t1.area,
    CASE
        WHEN place_name = 'カナルクルーザー' AND t1.area = 'タワーシティ' THEN 'カナルクルーザー(タワーシティ)'
        WHEN place_name = 'カナルクルーザー' AND t1.area = 'ウェルカムエリア' THEN 'カナルクルーザー(ウェルカムエリア)'
        ELSE place_name
    END AS place_name,
    5 AS buffer,
    CONCAT(id, CAST(TIMESTAMP(jst_date) AS STRING)) AS id_jst_date,
    latitude,
    longitude,
    t1.latitude AS beacon_lat,
    t1.longetude AS beacon_lon,
FROM
    `beaconbank-analytics.huistenbosch.20210705_huistenbosch_behavior_inside_park_v2` t0
    , `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon` t1
WHERE
    ST_DWITHIN(ST_GEOGPOINT(t0.longitude,t0.latitude), ST_GEOGPOINT(t1.longetude, t1.latitude),5)

UNION ALL

SELECT DISTINCT
    id,
    TIMESTAMP(jst_time),
    jst_date,
    hour,
    flag,
    visit_flag,
    repeat_flag,
    cnt_date,
    visit_per_month,
    home_flag,
    night_after,
    night_after,
    recent_visit,
    next_visit,
    CASE
      WHEN t0.Gender = 'M' THEN '男性'
      WHEN t0.Gender = 'F' THEN '女性'
      ELSE NULL
    END gender,
    CASE
      WHEN t0.Age = 10 THEN '10代'
      WHEN t0.Age = 20 THEN '20代'
      WHEN t0.Age = 30 THEN '30代'
      WHEN t0.Age = 40 THEN '40代'
      WHEN t0.Age = 50 THEN '50代'
      WHEN t0.Age = 60 THEN '60代'
      ELSE NULL
    END age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    'gps' AS data_from,
    NULL AS beacon_id,
    uuid,
    major,
    minor,
    t1.area,
    CASE
        WHEN place_name = 'カナルクルーザー' AND t1.area = 'タワーシティ' THEN 'カナルクルーザー(タワーシティ)'
        WHEN place_name = 'カナルクルーザー' AND t1.area = 'ウェルカムエリア' THEN 'カナルクルーザー(ウェルカムエリア)'
        ELSE place_name
    END AS place_name,
    3 AS buffer,
    CONCAT(id, CAST(TIMESTAMP(jst_date) AS STRING)) AS id_jst_date,
    latitude,
    longitude,
    t1.latitude AS beacon_lat,
    t1.longetude AS beacon_lon,
FROM
    `beaconbank-analytics.huistenbosch.20210629_huistenbosch_behavior_inside_park` t0
    , `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon` t1
WHERE
    ST_DWITHIN(ST_GEOGPOINT(t0.longitude,t0.latitude), ST_GEOGPOINT(t1.longetude, t1.latitude),3)
