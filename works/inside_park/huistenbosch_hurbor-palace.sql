WITH t0 AS(
SELECT  COUNT(DISTINCT CONCAT(id ,CAST(jst_date AS STRING))) AS denominator
FROM `beaconbank-analytics.huistenbosch.20210705_huistenbosch_routetree_beacon`
WHERE area = 'ハーバータウン'
)
,t1 AS(
SELECT COUNT(DISTINCT CONCAT(id ,CAST(jst_date AS STRING))) AS numerator
FROM `beaconbank-analytics.huistenbosch.20210705_huistenbosch_routetree_beacon`
WHERE area = 'ハーバータウン' AND place_name LIKE 'パレス%'
)
SELECT numerator, denominator, numerator / denominator AS ratio 
FROM t0, t1
  