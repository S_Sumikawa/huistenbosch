CREATE TABLE `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon_bu_20221025` AS
SELECT * FROM `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon`;

DELETE FROM `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon`
WHERE beacon_number BETWEEN 104 AND 106
;

INSERT INTO `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon`
SELECT
  *
FROM
  `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon_append_20220829`
;

-- beacon情報の追加
INSERT INTO `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon`
SELECT
  109	AS	beacon_number	,
  'huistenbosch-109'	AS	beacon_name	,
  '81D7FDC5-70B7-4475-821D-14E9F2312FCA'	AS	uuid	,
  181	AS	major	,
  117	AS	minor	,
  33.08630231	AS	latitude	,
  129.7903389	AS	longetude	,
  NULL	AS	area_bf	,
  '（IoTセンサー）VR-King①'	AS	place_name	,
  'アトラクションタウン'	AS	area	,
UNION ALL
SELECT
  110	AS	beacon_number	,
  'huistenbosch-110'	AS	beacon_name	,
  '81D7FDC5-70B7-4475-821D-14E9F2312FCA'	AS	uuid	,
  181	AS	major	,
  118	AS	minor	,
  33.08630231	AS	latitude	,
  129.7903389	AS	longetude	,
  NULL	AS	area_bf	,
  '（IoTセンサー）VR-King②'	AS	place_name	,
  'アトラクションタウン'	AS	area	,
;