/*分析 行動DNAの付与(etm:エンタメ)_一時テーブル*/
--ショップ・男女別のスコアの合計と人数を算出
--wiki url:
-- その他変更点(#numberでコード内検索)
    -- #0 出力する一時テーブル名
    -- #1 来訪者情報のテーブル名

create or replace table `beaconbank-analytics.huistenbosch.20210713_huistenbosch_all_dna` AS /*要変更点#0*/
-- INSERT INTO `beaconbank-analytics.huistenbosch.20210713_huistenbosch_all_dna`


WITH log AS(
    SELECT DISTINCT
--       t2.id,
      -- jst_date,
      -- id_jst_date,
      flag,
      home_flag,
      home_pref,
      gender,
      area,
      place_name,
      t1.*,
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT DISTINCT
            id,
            jst_date,
            id_jst_date,
            flag,
            home_flag,
            home_pref,
            gender,
            area,
            place_name,
        FROM 
            `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
) 
, unpivot_process_grm AS(
  -- GRM_CFS
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_CFS_Cafe' AS category, GRM_CFS_Cafe AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_CFS_JapaneseSweets' AS category, GRM_CFS_JapaneseSweets AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_CFS_OtherSweets' AS category, GRM_CFS_OtherSweets AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_CFS_WesternSweets' AS category, GRM_CFS_WesternSweets AS score FROM log
  
  -- GRM_FMR
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FMR_Chinese' AS category, GRM_FMR_Chinese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FMR_Japanese' AS category, GRM_FMR_Japanese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FMR_Sushi' AS category, GRM_FMR_Sushi AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FMR_Variety' AS category, GRM_FMR_Variety AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FMR_Western' AS category, GRM_FMR_Western AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FMR_Yakiniku' AS category, GRM_FMR_Yakiniku AS score FROM log
  
  -- GRM_FST
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_Bakery' AS category, GRM_FST_Bakery AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_Curry' AS category, GRM_FST_Curry AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_DonTeishoku' AS category, GRM_FST_DonTeishoku AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_HumbergerShop' AS category, GRM_FST_HumbergerShop AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_OtherFastFood' AS category, GRM_FST_OtherFastFood AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_Ramen' AS category, GRM_FST_Ramen AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_Takeout' AS category, GRM_FST_Takeout AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_FST_UdonSoba' AS category, GRM_FST_UdonSoba AS score FROM log
  
  -- GRM_OTR
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Asian' AS category, GRM_OTR_Asian AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Bar' AS category, GRM_OTR_Bar AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Chinese' AS category, GRM_OTR_Chinese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Ethnic' AS category, GRM_OTR_Ethnic AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_French' AS category, GRM_OTR_French AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Italian' AS category, GRM_OTR_Italian AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Izakaya' AS category, GRM_OTR_Izakaya AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_NabeOden' AS category, GRM_OTR_NabeOden AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_NigiriSushi' AS category, GRM_OTR_NigiriSushi AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Okonomiyaki' AS category, GRM_OTR_Okonomiyaki AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_OtherRestaurant' AS category, GRM_OTR_OtherRestaurant AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_PubClub' AS category, GRM_OTR_PubClub AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Steak' AS category, GRM_OTR_Steak AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Tonkatsu' AS category, GRM_OTR_Tonkatsu AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_TraditionalJapanese' AS category, GRM_OTR_TraditionalJapanese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Unagi' AS category, GRM_OTR_Unagi AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Yakiniku' AS category, GRM_OTR_Yakiniku AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'GRM_OTR_Yakitori' AS category, GRM_OTR_Yakitori AS score FROM log
)
,unpivot_process AS(
 SELECT * FROM unpivot_process_grm 
--  UNION ALL 
--  SELECT * FROM unpivot_process_etm 
--  UNION ALL 
-- SELECT * FROM unpivot_process_lfs 
--  UNION ALL 
--   SELECT * FROM unpivot_process_spg
)
/*スコアの合計と人数を算出*/
SELECT DISTINCT
  id,
  flag,
  home_flag,
  home_pref,
  gender,
  area,
  place_name,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  score
  -- SUM(score) AS sum_score,
  -- COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
-- GROUP BY
--   flag,
--   home_flag,
--   gender,
--   category,
--   Cat_Lv1_jp,
--   Cat_Lv2_jp,
--   Cat_Lv3_jp
;


INSERT INTO `beaconbank-analytics.huistenbosch.20210713_huistenbosch_all_dna`


WITH log AS(
    SELECT DISTINCT
--       t2.id,
      -- jst_date,
      -- id_jst_date,
      flag,
      home_flag,
      home_pref,
      gender,
      area,
      place_name,
      t1.*,
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT DISTINCT
            id,
            jst_date,
            id_jst_date,
            flag,
            home_flag,
            home_pref,
            gender,
            area,
            place_name,
        FROM 
            `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
) 
,unpivot_process_etm AS(
  -- ETM_ACM
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_BusinessHotel' AS category, ETM_ACM_BusinessHotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_Hotel' AS category, ETM_ACM_Hotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_JapaneseHotel' AS category, ETM_ACM_JapaneseHotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_LeisureHotel' AS category, ETM_ACM_LeisureHotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_Minshuku' AS category, ETM_ACM_Minshuku AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_OtherAccomodation' AS category, ETM_ACM_OtherAccomodation AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_Pension' AS category, ETM_ACM_Pension AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_ACM_PublicHotel' AS category, ETM_ACM_PublicHotel AS score FROM log

  -- ETM_AMS
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_AMS_Arcade' AS category, ETM_AMS_Arcade AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_AMS_InternetComicCafe' AS category, ETM_AMS_InternetComicCafe AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_AMS_Karaoke' AS category, ETM_AMS_Karaoke AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_AMS_Pachinko' AS category, ETM_AMS_Pachinko AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_AMS_Themepark' AS category, ETM_AMS_Themepark AS score FROM log
  
  -- ETM_APR
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_Aquarium' AS category, ETM_APR_Aquarium AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_Archive' AS category, ETM_APR_Archive AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_ArtMuseum' AS category, ETM_APR_ArtMuseum AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_Hall' AS category, ETM_APR_Hall AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_LiveHouse' AS category, ETM_APR_LiveHouse AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_Museum' AS category, ETM_APR_Museum AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_OtherAppriciation' AS category, ETM_APR_OtherAppriciation AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_Theater' AS category, ETM_APR_Theater AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_APR_ZooBotanical' AS category, ETM_APR_ZooBotanical AS score FROM log
  
  -- ETM_OTR, ETM_PRK, ETM_SPA
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_OTR_OtherEntertainment' AS category, ETM_OTR_OtherEntertainment AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_PRK_Park' AS category, ETM_PRK_Park AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPA_Spa' AS category, ETM_SPA_Spa AS score FROM log
  
  -- ETM_SPO
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_Ballpark' AS category, ETM_SPO_Ballpark AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_Budo' AS category, ETM_SPO_Budo AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_CyclingRoad' AS category, ETM_SPO_CyclingRoad AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_FitnessGym' AS category, ETM_SPO_FitnessGym AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_Golf' AS category, ETM_SPO_Golf AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_OtherSports' AS category, ETM_SPO_OtherSports AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_Pool' AS category, ETM_SPO_Pool AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_SnowMountain' AS category, ETM_SPO_SnowMountain AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_Stadium' AS category, ETM_SPO_Stadium AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SPO_Tennis' AS category, ETM_SPO_Tennis AS score FROM log
  
  -- ETM_SS
  UNION ALL
  SELECT id, flag, home_flag, home_pref,  gender,area,place_name, 'ETM_SS_SightSpot' AS category, ETM_SS_SightSpot AS score FROM log
)
,unpivot_process AS(
--  SELECT * FROM unpivot_process_grm 
--  UNION ALL 
 SELECT * FROM unpivot_process_etm 
--  UNION ALL 
-- SELECT * FROM unpivot_process_lfs 
--  UNION ALL 
--   SELECT * FROM unpivot_process_spg
)
/*スコアの合計と人数を算出*/
SELECT DISTINCT
  id,
  flag,
  home_flag,
  home_pref,
  gender,
  area,
  place_name,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  score
  -- SUM(score) AS sum_score,
  -- COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
-- GROUP BY
--   flag,
--   home_flag,
--   gender,
--   category,
--   Cat_Lv1_jp,
--   Cat_Lv2_jp,
--   Cat_Lv3_jp
;
INSERT INTO `beaconbank-analytics.huistenbosch.20210713_huistenbosch_all_dna`


WITH log AS(
    SELECT DISTINCT
--       t2.id,
      -- jst_date,
      -- id_jst_date,
      flag,
      home_flag,
      home_pref,
      gender,
      area,
      place_name,
      t1.*,
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT DISTINCT
            id,
            jst_date,
            id_jst_date,
            flag,
            home_flag,
            home_pref,
            gender,
            area,
            place_name,
        FROM 
            `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
) 
, unpivot_process_lfs AS(
  -- LFS_EDU
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_ElementarySchool' AS category, LFS_EDU_ElementarySchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_HighSchool' AS category, LFS_EDU_HighSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_Hndicapped' AS category, LFS_EDU_Hndicapped AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_JuniorCollege' AS category, LFS_EDU_JuniorCollege AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_JuniorHighSchool' AS category, LFS_EDU_JuniorHighSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_KindergartenNursery' AS category, LFS_EDU_KindergartenNursery AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_PrepSchool' AS category, LFS_EDU_PrepSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_TechnicalCollege' AS category, LFS_EDU_TechnicalCollege AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_Univercity' AS category, LFS_EDU_Univercity AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_EDU_VocationalSchool' AS category, LFS_EDU_VocationalSchool AS score FROM log

  -- LFS_FIN, LFS_HSP は使わない

  -- LFS_EDU
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_OTR_BeautySalons' AS category, LFS_OTR_BeautySalons AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_OTR_Laundry' AS category, LFS_OTR_Laundry AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_OTR_Massage' AS category, LFS_OTR_Massage AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_OTR_PetService' AS category, LFS_OTR_PetService AS score FROM log

  -- LFS_PUB は使わない

  -- LFS_SCL
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_AbacusShool' AS category, LFS_SCL_AbacusShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_BalletShool' AS category, LFS_SCL_BalletShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_BusinessSchool' AS category, LFS_SCL_BusinessSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_CalligraphyShool' AS category, LFS_SCL_CalligraphyShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_CeramicArtShool' AS category, LFS_SCL_CeramicArtShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_ComputerShool' AS category, LFS_SCL_ComputerShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_CookingShool' AS category, LFS_SCL_CookingShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_DanceShool' AS category, LFS_SCL_DanceShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_DressingShool' AS category, LFS_SCL_DressingShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_English' AS category, LFS_SCL_English AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_FlowerArrangementShool' AS category, LFS_SCL_FlowerArrangementShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_ForeignLang' AS category, LFS_SCL_ForeignLang AS score FROM log
   UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_GlassCraftShool' AS category, LFS_SCL_GlassCraftShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_KnittingShool' AS category, LFS_SCL_KnittingShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_MusicShool' AS category, LFS_SCL_MusicShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_PaintingArtShool' AS category, LFS_SCL_PaintingArtShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_PianoShool' AS category, LFS_SCL_PianoShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_SCL_TeaCeremonyShool' AS category, LFS_SCL_TeaCeremonyShool AS score FROM log

  -- LFS_TRS （LFS_TRS_CarCareService, LFS_TRS_FerryTerminal は除外する）
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_Airport' AS category, LFS_TRS_Airport AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_BusTerminal' AS category, LFS_TRS_BusTerminal AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_Carpool' AS category, LFS_TRS_Carpool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_GasStation' AS category, LFS_TRS_GasStation AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_RentaCar' AS category, LFS_TRS_RentaCar AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_RoadsideRest' AS category, LFS_TRS_RoadsideRest AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_SAPA' AS category, LFS_TRS_SAPA AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'LFS_TRS_Station' AS category, LFS_TRS_Station AS score FROM log
)
,unpivot_process AS(
--  SELECT * FROM unpivot_process_grm 
--  UNION ALL 
--  SELECT * FROM unpivot_process_etm 
--  UNION ALL 
SELECT * FROM unpivot_process_lfs 
--  UNION ALL 
--   SELECT * FROM unpivot_process_spg
)
/*スコアの合計と人数を算出*/
SELECT DISTINCT
  id,
  flag,
  home_flag,
  home_pref,
  gender,
  area,
  place_name,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  score
  -- SUM(score) AS sum_score,
  -- COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
-- GROUP BY
--   flag,
--   home_flag,
--   gender,
--   category,
--   Cat_Lv1_jp,
--   Cat_Lv2_jp,
--   Cat_Lv3_jp
;
INSERT INTO `beaconbank-analytics.huistenbosch.20210713_huistenbosch_all_dna`


WITH log AS(
    SELECT DISTINCT
--       t2.id,
      -- jst_date,
      -- id_jst_date,
      flag,
      home_flag,
      home_pref,
      gender,
      area,
      place_name,
      t1.*,
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT DISTINCT
            id,
            jst_date,
            id_jst_date,
            flag,
            home_flag,
            home_pref,
            gender,
            area,
            place_name,
        FROM 
            `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
) 
, unpivot_process_spg AS(
  -- SPG_APL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_Accessory' AS category, SPG_APL_Accessory AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_BabyChildren' AS category, SPG_APL_BabyChildren AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_Bag' AS category, SPG_APL_Bag AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_FastFashion' AS category, SPG_APL_FastFashion AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_FormalWear' AS category, SPG_APL_FormalWear AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_Jewelry' AS category, SPG_APL_Jewelry AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_Kimono' AS category, SPG_APL_Kimono AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_Lingerie' AS category, SPG_APL_Lingerie AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_SecondHand' AS category, SPG_APL_SecondHand AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_ShoeStore' AS category, SPG_APL_ShoeStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_APL_Watch' AS category, SPG_APL_Watch AS score FROM log
  
  -- SPG_BIG
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_DepartmentStore' AS category, SPG_BIG_DepartmentStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_FurnitureStore' AS category, SPG_BIG_FurnitureStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_HomeApplianceStore' AS category, SPG_BIG_HomeApplianceStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_HomeCenter' AS category, SPG_BIG_HomeCenter AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_MarketStreet' AS category, SPG_BIG_MarketStreet AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_MiscellaneousGoodsStore' AS category, SPG_BIG_MiscellaneousGoodsStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_OtherSC' AS category, SPG_BIG_OtherSC AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_OutletMall' AS category, SPG_BIG_OutletMall AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_RecycleShop' AS category, SPG_BIG_RecycleShop AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_BIG_ShoppingMallComplex' AS category, SPG_BIG_ShoppingMallComplex AS score FROM log
  
  -- SPG_BIG
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SML_ConvenienceStore' AS category, SPG_SML_ConvenienceStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SML_DiscountStore' AS category, SPG_SML_DiscountStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SML_DrugStore' AS category, SPG_SML_DrugStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SML_FoodMarket' AS category, SPG_SML_FoodMarket AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SML_MiniMarket' AS category, SPG_SML_MiniMarket AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SML_OneDollerMarket' AS category, SPG_SML_OneDollerMarket AS score FROM log
  
  -- SPG_SPC
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_BookStore' AS category, SPG_SPC_BookStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_CarDealer' AS category, SPG_SPC_CarDealer AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_CarSupplyStore' AS category, SPG_SPC_CarSupplyStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_GolfStore' AS category, SPG_SPC_GolfStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_HousingExpo' AS category, SPG_SPC_HousingExpo AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_LiquorStore' AS category, SPG_SPC_LiquorStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_MobileStore' AS category, SPG_SPC_MobileStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_OtherSpecialityStore' AS category, SPG_SPC_OtherSpecialityStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_OutdoorGoodsStore' AS category, SPG_SPC_OutdoorGoodsStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_PreOwnedCarDealer' AS category, SPG_SPC_PreOwnedCarDealer AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_RentVideoCD' AS category, SPG_SPC_RentVideoCD AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_SportsGoodsStore' AS category, SPG_SPC_SportsGoodsStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, home_pref,  gender,area,place_name, 'SPG_SPC_Toy' AS category, SPG_SPC_Toy AS score FROM log
)
,unpivot_process AS(
--  SELECT * FROM unpivot_process_grm 
--  UNION ALL 
--  SELECT * FROM unpivot_process_etm 
--  UNION ALL 
-- SELECT * FROM unpivot_process_lfs 
--  UNION ALL 
  SELECT * FROM unpivot_process_spg
)
/*スコアの合計と人数を算出*/
SELECT DISTINCT
  id,
  flag,
  home_flag,
  home_pref,
  gender,
  area,
  place_name,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  score
  -- SUM(score) AS sum_score,
  -- COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
-- GROUP BY
--   flag,
--   home_flag,
--   gender,
--   category,
--   Cat_Lv1_jp,
--   Cat_Lv2_jp,
--   Cat_Lv3_jp
;