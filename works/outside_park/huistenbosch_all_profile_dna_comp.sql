CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210618_huistenbosch_all_profile_dna_comp_v2` AS

WITH t0 AS(
    SELECT
        flag,
        home_flag,
        AVG(ave_score) AS AVG_total,
        AVG(male_ave_score) AS M_AVG_total,
        AVG(female_ave_score) AS F_AVG_total,
        50/AVG(ave_score) AS comp_total,
        50/AVG(male_ave_score) AS comp_M,
        50/AVG(female_ave_score) AS comp_F
    FROM 
        `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_dna`
    GROUP BY
        1,2
)
SELECT
    flag,
    home_flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    ave_score * comp_total AS ave_score,
    male_ave_score * comp_M AS male_ave_score,
    female_ave_score * comp_F AS female_ave_score
FROM 
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_dna`
    LEFT JOIN t0 USING(flag,home_flag)


-- CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210618_huistenbosch_all_profile_dna_all_comp_v2` AS

-- WITH t0 AS(
--     SELECT
--         flag,
--         AVG(ave_score) AS AVG_total,
--         AVG(male_ave_score) AS M_AVG_total,
--         AVG(female_ave_score) AS F_AVG_total,
--         50/AVG(ave_score) AS comp_total,
--         50/AVG(male_ave_score) AS comp_M,
--         50/AVG(female_ave_score) AS comp_F
--     FROM 
--         `beaconbank-analytics.huistenbosch.20210616_huistenbosch_all_profile_dna_all`
--     GROUP BY
--         1
-- )
-- SELECT
--     flag,
--     category,
--     Cat_Lv1_jp,
--     Cat_Lv2_jp,
--     Cat_Lv3_jp,
--     ave_score * comp_total AS ave_score,
--     male_ave_score * comp_M AS male_ave_score,
--     female_ave_score * comp_F AS female_ave_score
-- FROM 
--     `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_dna_all`
--     LEFT JOIN t0 USING(flag)