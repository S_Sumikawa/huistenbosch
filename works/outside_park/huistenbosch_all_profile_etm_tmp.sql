/*分析 行動DNAの付与(etm:エンタメ)_一時テーブル*/
--ショップ・男女別のスコアの合計と人数を算出
--wiki url:
-- その他変更点(#numberでコード内検索)
    -- #0 出力する一時テーブル名
    -- #1 来訪者情報のテーブル名

create or replace table `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_etm_tmp` AS /*要変更点#0*/

WITH log AS(
  SELECT
    t1.*,
    CASE
      WHEN t2.Gender = 'M' THEN '男性'
      WHEN t2.Gender = 'F' THEN '女性'
      ELSE NULL
      END gender
  FROM(
    SELECT DISTINCT
      flag,
      home_flag,
      t1.*,
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT 
            *
        FROM 
            `beaconbank-analytics.huistenbosch.20210608_huistenbosch_all_profile` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
  ) t1
  LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_gender` t2 USING(id)
) ,
unpivot_process AS(
  -- ETM_ACM
  SELECT id, flag, home_flag, gender, 'ETM_ACM_BusinessHotel' AS category, ETM_ACM_BusinessHotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_ACM_Hotel' AS category, ETM_ACM_Hotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_ACM_JapaneseHotel' AS category, ETM_ACM_JapaneseHotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_ACM_LeisureHotel' AS category, ETM_ACM_LeisureHotel AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_ACM_Minshuku' AS category, ETM_ACM_Minshuku AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_ACM_OtherAccomodation' AS category, ETM_ACM_OtherAccomodation AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_ACM_Pension' AS category, ETM_ACM_Pension AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_ACM_PublicHotel' AS category, ETM_ACM_PublicHotel AS score FROM log

  -- ETM_AMS
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_AMS_Arcade' AS category, ETM_AMS_Arcade AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_AMS_InternetComicCafe' AS category, ETM_AMS_InternetComicCafe AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_AMS_Karaoke' AS category, ETM_AMS_Karaoke AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_AMS_Pachinko' AS category, ETM_AMS_Pachinko AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_AMS_Themepark' AS category, ETM_AMS_Themepark AS score FROM log
  
  -- ETM_APR
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_Aquarium' AS category, ETM_APR_Aquarium AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_Archive' AS category, ETM_APR_Archive AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_ArtMuseum' AS category, ETM_APR_ArtMuseum AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_Hall' AS category, ETM_APR_Hall AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_LiveHouse' AS category, ETM_APR_LiveHouse AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_Museum' AS category, ETM_APR_Museum AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_OtherAppriciation' AS category, ETM_APR_OtherAppriciation AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_Theater' AS category, ETM_APR_Theater AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_APR_ZooBotanical' AS category, ETM_APR_ZooBotanical AS score FROM log
  
  -- ETM_OTR, ETM_PRK, ETM_SPA
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_OTR_OtherEntertainment' AS category, ETM_OTR_OtherEntertainment AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_PRK_Park' AS category, ETM_PRK_Park AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPA_Spa' AS category, ETM_SPA_Spa AS score FROM log
  
  -- ETM_SPO
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_Ballpark' AS category, ETM_SPO_Ballpark AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_Budo' AS category, ETM_SPO_Budo AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_CyclingRoad' AS category, ETM_SPO_CyclingRoad AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_FitnessGym' AS category, ETM_SPO_FitnessGym AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_Golf' AS category, ETM_SPO_Golf AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_OtherSports' AS category, ETM_SPO_OtherSports AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_Pool' AS category, ETM_SPO_Pool AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_SnowMountain' AS category, ETM_SPO_SnowMountain AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_Stadium' AS category, ETM_SPO_Stadium AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SPO_Tennis' AS category, ETM_SPO_Tennis AS score FROM log
  
  -- ETM_SS
  UNION ALL
  SELECT id, flag, home_flag, gender, 'ETM_SS_SightSpot' AS category, ETM_SS_SightSpot AS score FROM log
)

/*スコアの合計と人数を算出*/
SELECT
  flag,
  home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  SUM(score) AS sum_score,
  COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
GROUP BY
  flag,
  home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp