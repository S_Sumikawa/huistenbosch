CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210608_huistenbosch_all_profile` AS

SELECT DISTINCT
    id,
--     visit_date,
    pr.cnt_date,
    visit_per_month,
    pr.flag,
--     repeat_flag,
--     trip_flag,
    home_flag,
--     mode,
--     ratio_ship,
--     ratio_train,
--     ratio_car,
--     night_before,
--     night_after,
--     recent_visit,
--     next_visit,
    pr.Gender,
    pr.Age,
    pr.home_lat,
    pr.home_lon,
    pr.home_pref,
    pr.home_city,
    pr.office_pref,
    pr.office_city
FROM
    `beaconbank-analytics.huistenbosch.20210607_huistenbosch_profile_v8` pr
    -- LEFT JOIN `beaconbank-analytics.huistenbosch.20210607_huistenbosch_travel_behavior` tb USING(id)
    -- LEFT JOIN `beaconbank-analytics.huistenbosch.20210607_huistenbosch_06_Trans_Ratio` tr USING(id)

--DNA作成時はみんなのクエリのDNA付与で以下変更


create or replace table `beaconbank-analytics.huistenbosch.20210608_huuistenbosch_all_profile_etm_tmp` AS /*要変更点#0*/

JOIN `beaconbank-analytics.huistenbosch.20210607_huistenbosch_all_profile` t2 /*要変更点#1*/