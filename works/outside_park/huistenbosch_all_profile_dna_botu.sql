/*分析 行動DNAの付与_集計*/
--一時テーブルの値を集計して、ショップ別の行動DNA、ショップ・男女別の行動DNAを算出
--wiki url:
-- その他変更点(#numberでコード内検索)
    -- #0 出力するテーブル名
    -- #1 DNA情報の一時テーブル名

create or replace table `beaconbank-analytics.huistenbosch.20210608_huuistenbosch_all_profile_dna` AS /*要変更点#0*/

WITH dna_etm AS( /*男女別の行動DNA*/
  SELECT
    flag,
    home_flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    FIRST_VALUE(sum_score) OVER(ORDER BY gender) /FIRST_VALUE(uu) OVER(ORDER BY gender) AS female_ave_score,
    LAST_VALUE(sum_score) OVER(ORDER BY gender) /LAST_VALUE(uu) OVER(ORDER BY gender) AS male_ave_score,
    SUM(sum_score) OVER(PARTITION BY 1,2,3,4,5,6) /SUM(uu) OVER(PARTITION BY 1,2,3,4,5,6) AS ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210608_huuistenbosch_all_profile_etm_tmp` /*要変更点#1*/
  PIVOT( SUM(Number) FOR gender IN ('M' AS male,
  'F' AS female))
)
, dna_grm AS( /*男女別の行動DNA*/
  SELECT
    flag,
    home_flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    FIRST_VALUE(sum_score) OVER(ORDER BY gender) /FIRST_VALUE(uu) OVER(ORDER BY gender) AS female_ave_score,
    LAST_VALUE(sum_score) OVER(ORDER BY gender) /LAST_VALUE(uu) OVER(ORDER BY gender) AS male_ave_score,
    SUM(sum_score) OVER(PARTITION BY 1,2,3,4,5,6) /SUM(uu) OVER(PARTITION BY 1,2,3,4,5,6) AS ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210608_huuistenbosch_all_profile_grm_tmp` /*要変更点#1*/
)
, dna_lfs AS( /*男女別の行動DNA*/
  SELECT
    flag,
    home_flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    FIRST_VALUE(sum_score) OVER(ORDER BY gender) /FIRST_VALUE(uu) OVER(ORDER BY gender) AS female_ave_score,
    LAST_VALUE(sum_score) OVER(ORDER BY gender) /LAST_VALUE(uu) OVER(ORDER BY gender) AS male_ave_score,
    SUM(sum_score) OVER(PARTITION BY 1,2,3,4,5,6) /SUM(uu) OVER(PARTITION BY 1,2,3,4,5,6) AS ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210608_huuistenbosch_all_profile_lfs_tmp` /*要変更点#1*/
)
, dna_spg AS( /*男女別の行動DNA*/
  SELECT
    flag,
    home_flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    FIRST_VALUE(sum_score) OVER(ORDER BY gender) /FIRST_VALUE(uu) OVER(ORDER BY gender) AS female_ave_score,
    LAST_VALUE(sum_score) OVER(ORDER BY gender) /LAST_VALUE(uu) OVER(ORDER BY gender) AS male_ave_score,
    SUM(sum_score) OVER(PARTITION BY 1,2,3,4,5,6) /SUM(uu) OVER(PARTITION BY 1,2,3,4,5,6) AS ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210608_huuistenbosch_all_profile_spg_tmp` /*要変更点#1*/
)

SELECT * FROM dna_etm
UNION ALL SELECT * FROM dna_grm
UNION ALL SELECT * FROM dna_lfs
UNION ALL SELECT * FROM dna_spg