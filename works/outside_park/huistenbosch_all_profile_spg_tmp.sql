/*分析 行動DNAの付与(spg:ショッピング)*/
--ショップ・男女別のスコアの合計と人数を算出
--wiki url:
-- その他変更点(#numberでコード内検索)
    -- #0 出力する一時テーブル名
    -- #1 来訪者情報のテーブル名

create or replace table `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_spg_tmp` AS /*要変更点#0*/

WITH log AS(
  SELECT
    t1.*,
    CASE
      WHEN t2.Gender = 'M' THEN '男性'
      WHEN t2.Gender = 'F' THEN '女性'
      ELSE NULL
      END gender
  FROM(
    SELECT DISTINCT
      flag, home_flag,
      t1.*
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT 
            *
        FROM 
            `beaconbank-analytics.huistenbosch.20210608_huistenbosch_all_profile` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
  ) t1
  LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_gender` t2 USING(id)
) ,
unpivot_process AS(
  -- SPG_APL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_Accessory' AS category, SPG_APL_Accessory AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_BabyChildren' AS category, SPG_APL_BabyChildren AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_Bag' AS category, SPG_APL_Bag AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_FastFashion' AS category, SPG_APL_FastFashion AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_FormalWear' AS category, SPG_APL_FormalWear AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_Jewelry' AS category, SPG_APL_Jewelry AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_Kimono' AS category, SPG_APL_Kimono AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_Lingerie' AS category, SPG_APL_Lingerie AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_SecondHand' AS category, SPG_APL_SecondHand AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_ShoeStore' AS category, SPG_APL_ShoeStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_APL_Watch' AS category, SPG_APL_Watch AS score FROM log
  
  -- SPG_BIG
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_DepartmentStore' AS category, SPG_BIG_DepartmentStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_FurnitureStore' AS category, SPG_BIG_FurnitureStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_HomeApplianceStore' AS category, SPG_BIG_HomeApplianceStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_HomeCenter' AS category, SPG_BIG_HomeCenter AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_MarketStreet' AS category, SPG_BIG_MarketStreet AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_MiscellaneousGoodsStore' AS category, SPG_BIG_MiscellaneousGoodsStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_OtherSC' AS category, SPG_BIG_OtherSC AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_OutletMall' AS category, SPG_BIG_OutletMall AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_RecycleShop' AS category, SPG_BIG_RecycleShop AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_BIG_ShoppingMallComplex' AS category, SPG_BIG_ShoppingMallComplex AS score FROM log
  
  -- SPG_BIG
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SML_ConvenienceStore' AS category, SPG_SML_ConvenienceStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SML_DiscountStore' AS category, SPG_SML_DiscountStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SML_DrugStore' AS category, SPG_SML_DrugStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SML_FoodMarket' AS category, SPG_SML_FoodMarket AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SML_MiniMarket' AS category, SPG_SML_MiniMarket AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SML_OneDollerMarket' AS category, SPG_SML_OneDollerMarket AS score FROM log
  
  -- SPG_SPC
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_BookStore' AS category, SPG_SPC_BookStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_CarDealer' AS category, SPG_SPC_CarDealer AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_CarSupplyStore' AS category, SPG_SPC_CarSupplyStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_GolfStore' AS category, SPG_SPC_GolfStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_HousingExpo' AS category, SPG_SPC_HousingExpo AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_LiquorStore' AS category, SPG_SPC_LiquorStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_MobileStore' AS category, SPG_SPC_MobileStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_OtherSpecialityStore' AS category, SPG_SPC_OtherSpecialityStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_OutdoorGoodsStore' AS category, SPG_SPC_OutdoorGoodsStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_PreOwnedCarDealer' AS category, SPG_SPC_PreOwnedCarDealer AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_RentVideoCD' AS category, SPG_SPC_RentVideoCD AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_SportsGoodsStore' AS category, SPG_SPC_SportsGoodsStore AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'SPG_SPC_Toy' AS category, SPG_SPC_Toy AS score FROM log
)

SELECT
  flag, home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  SUM(score) AS sum_score,
  COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
GROUP BY
  flag, home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp