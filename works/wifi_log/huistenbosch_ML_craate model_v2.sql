CREATE OR REPLACE MODEL
  `beaconbank-analytics.huistenbosch.VISIT_MODEL_RP_1611307908901850490_v2` OPTIONS (model_type='linear_reg',
    l1_reg=1,
    l2_reg=2,
    data_split_method="no_split",
    max_iterations=50,
    ls_init_learn_rate=0.1) AS
SELECT
  rc.*,
  tr._1day_visitor AS LABEL
FROM
  `beaconbank-analytics.huistenbosch.20210709_huistenbosch_wifi_ML_rssi_count` rc
INNER JOIN
  (SELECT datetime, day, weahter, visitor, visitor_foreign, _1day_visitor FROM `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_ML_trainingdata_daily_v2`
  UNION ALL
  SELECT * FROM `labs-analytics-298709.works_sumikawa.20210709_huistenbosch_wifi_ML_trainingdate_daily_june`
  )
   tr
   ON 
-- tr.device_id  = rc.DEVICE_ID AND
   tr.datetime = rc.DATETIME
--    AND cast(tr.hour as STRING) = rc.hour 
-- WHERE rc.DATE <= "2020-10-29"

