CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_log_for_ML_v2` AS
-- WITH sum_logcnt AS(
SELECT  *
-- 	device_id,
--   datetime,
--   hour,
--   SUM(cnt) AS sum_cnt
FROM `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_log_for_ML`
WHERE 
((hour <= 8 OR hour >= 22) 
OR (datetime NOT IN (20210403,20210410,20210417) AND hour >= 21) 
OR datetime = 20210406
OR datetime = 20210412 )
IS NOT TRUE
-- GROUP BY 1,2
-- ),
-- ratio AS(
-- SELECT  
-- 	device_id,
--   datetime,
--   hour,
--   cnt / sum_cnt AS ratio
-- FROM `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_log_for_ML`
-- JOIN sum_logcnt USING(device_id, datetime)
-- WHERE 
-- ((hour <= 8 OR hour >= 22) 
-- OR (datetime NOT IN (20210403,20210410,20210417) AND hour >= 21) )
-- IS NOT TRUE
-- GROUP BY 1,2
-- )

-- SELECT DISTINCT *
-- FROM ratio
ORDER BY 1,2,3