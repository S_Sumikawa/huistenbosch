CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210705_huistenbosch_gender_ratio` AS 

with t0 AS( 
SELECT 
  gender,
  area,
  place_name,
  count(distinct id) AS cnt 
FROM 
    `20210705_huistenbosch_beacon_visitor_v3`  
GROUP BY 
  1
)
, t1 AS(
SELECT  
  gender, 
  cnt, 
  sum(cnt) OVER() AS sum,
  cnt /sum(cnt) OVER() AS ratio 
FROM 
  t0
WHERE 
  gender IS NOT NULL
)
SELECT
  gender,
  cnt AS cnt_uu,
  ratio AS ratio_bf,
              CASE	
                    WHEN gender = 'M' THEN ratio * 	0.792900188
                    WHEN gender = 'F' THEN ratio * 	1.223552495
             END AS ratio_compensated	
FROM t1

            
