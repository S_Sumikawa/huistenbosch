/* 分析_placelistを用いたログの抽出 */
-- 1-３月の九州以外来訪者の来園前後2日間の立ち寄り施設


-- ☆☆☆以下は設定なので書き換えないこと☆☆☆
DECLARE 
nh_start,
nh_end INT64; 

-- ★★★設定項目★★★

-- 夜間の時間帯(ex.23~5)
-- nh = night_hour
SET nh_start = 23;
SET nh_end = 5;

-- ★★★設定終了★★★

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.tmp_202201_03_visitor_travel_poi` AS /*要変更点#0*/

WITH place AS ( 
  SELECT
    place_id,
    place_name,
    chain_name,
    cat_tmp,
    cat_lv1_jp,
    cat_lv2_jp,
    cat_lv3_jp,
    pl_lat,
    pl_lon,
    radius,
    prefecture,
    (pl_lat - (500 / 30.8184 * 0.000277778)) AS lat_min,
    (pl_lat + (500 / 30.8184 * 0.000277778)) AS lat_max,
    (pl_lon - (500 / 25.2450 * 0.000277778)) AS lon_min,
    (pl_lon + (500 / 25.2450 * 0.000277778)) AS lon_max
  FROM
    `beaconbank-analytics.huistenbosch.20210604_huistenbosch_target_places`
  GROUP BY
    place_id,
    place_name,
    chain_name,
    cat_tmp,
    cat_lv1_jp,
    cat_lv2_jp,
    cat_lv3_jp,
    pl_lat,
    pl_lon,
    radius,
    prefecture
) 
, logs AS(
    SELECT 
        *,
        EXTRACT(MONTH FROM jst_date) AS month,
    FROM 
        `beaconbank-analytics.huistenbosch.tmp_202201_03_visitor_travel_heatmap`
    -- WHERE
    --     trip_flag = 1
)
, count_visitor AS(
  SELECT 
    EXTRACT(MONTH FROM jst_date) AS month,
    -- COUNT(DISTINCT CONCAT( id, visit_date)) AS cnt_visitor
    COUNT(DISTINCT CONCAT( id, jst_date)) AS cnt_visitor_dau,
    COUNT(DISTINCT CONCAT( id)) AS cnt_visitor_uu
  FROM 
      `beaconbank-analytics.huistenbosch.tmp_202201_03_visitor_travel_heatmap`
  -- WHERE
    -- hour > nh_end 
    -- AND hour < nh_start
  GROUP BY 1
)

, tmp AS(
  SELECT
  DISTINCT t0.id AS id,
  jst_date,
  cat_tmp,
  place_name,
  month,
  -- visit_flag,
  -- cnt_date,
  -- night_before,
  -- night_after,
  -- recent_visit,
  -- next_visit,
  Gender,
  Age,
  home_pref,
  home_city,
  office_pref,
  office_city
FROM
  (
    /* 説明)業態によって推奨抽出半径が異なる。抽出半径ごとに処理することで処理スピードが上がる */
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),25)
      AND radius = 25
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),30)
      AND radius = 30
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),50)
      AND radius = 50
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),80)
      AND radius = 80
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),100)
      AND radius = 100
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),150)
      AND radius = 150
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),200)
      AND radius = 200
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),250)
      AND radius = 250
  ) t0
)
SELECT
  cat_tmp,
  place_name,
  month,
  cnt_visitor_dau,
  cnt_visitor_uu,
  COUNT(DISTINCT CONCAT(id,jst_date)) AS cnt_dau,
  COUNT(DISTINCT CONCAT(id,jst_date)) / cnt_visitor_dau AS ratio_dau,
  COUNT(DISTINCT CONCAT(id)) AS cnt_uu,
  COUNT(DISTINCT CONCAT(id)) / cnt_visitor_uu AS ratio_uu
FROM
  tmp
  JOIN count_visitor USING(month)
GROUP BY
  1,2,3,4,5
  -- 1,2,3