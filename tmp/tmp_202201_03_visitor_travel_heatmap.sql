-- version_202112
/*分析 来訪日テーブル作成_緯度経度_テーブルから入力_2期間選択*/
/*analysis_visitor_latlon_table*/
-- 1-３月の九州以外来訪者の来園前後2日間のヒートマップ

-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
  DECLARE start_date1,end_date1,start_date2,end_date2,separate_date1,separate_date2 DATE;
  DECLARE lat1,lat2,lon1,lon2,radius INT64;
  DECLARE filter_geo, data_source ARRAY<STRING>;
  DECLARE geo_length INT64;
  DECLARE target_poly,target_poly2 STRING;
-- ログ検索範囲設定用(原則書き換えない)
  SET lat1 = 23;
  SET lat2 = 46;
  SET lon1 = 123;
  SET lon2 = 148;

-- ★★★ 変数設定 ★★★
-- *** settings ***
  -- ターゲット期間
  -- target period
    -- SET start_date1 = '2019-09-01';
    -- SET end_date1 = '2019-12-31';
    -- SET separate_date1 = '2019-11-01'; -- クリスマス期間開始日
  -- target period
    SET start_date2 = '2022-01-01';
    SET end_date2 = '2022-03-31';
    -- SET separate_date2 = '2021-11-06'; -- クリスマス期間開始日
  -- データソースの指定
  -- target data sorces
      -- bb_beacon:   BeaconBankのビーコンログ
      -- bb_location: BeaconBankのGPSログ
      -- pd:          predicioのGPSログ
      -- vs:          verasetのGPSログ
      -- bw_radiko:   radikoのGPSログ
    SET data_source = ['bb_beacon','bb_location','vs'];

    -- SET target_poly = "POLYGON((129.78205143090764 33.07835732316272,129.78219090577642 33.07812358391031,129.78372512933294 33.0782629285394,129.78587626096288 33.07994094642577,129.78602110024968 33.08198610156933,129.78632687207738 33.08196812239075,129.78637515183965 33.08225578880849,129.7859398408834 33.082355053138926,129.78600113514284 33.08309912492111,129.78626627728738 33.083227414448146,129.78640953038254 33.08304388872046,129.78917976212063 33.08287011053718,129.78980105426348 33.08414065430174,129.79152107175383 33.08433100348544,129.79110945458916 33.08502632589806,129.7910586964561 33.08620121471334,129.7913380538701 33.087036299631215,129.79091508019866 33.08714236290895,129.79166772942864 33.08871182359587,129.79188661906466 33.08888124879444,129.7924702891267 33.08883494061111,129.7925948291466 33.08998005584629,129.78986665717986 33.090216336850176,129.7891685520388 33.090225977745845,129.7890900371802 33.090963703982716,129.78769187548625 33.09082952596329,129.78692805622433 33.09023917012475,129.78641100019178 33.08956791103889,129.78771577438766 33.08878269066757,129.78662631556818 33.087610355058665,129.78576168034652 33.08756685566208,129.7858348543612 33.08664836788825,129.7855486123679 33.08584448281953,129.78529455688277 33.08520239722506,129.7836560624508 33.08432402190212,129.7840966152815 33.08369092888635,129.7829821574358 33.08192131330081,129.78253959294835 33.08009005051815,129.7823303806452 33.07992900685026,129.78205143090764 33.07835732316272))";
    -- SET radius = 10000;
    -- -- ロッテルダム特設会場
    -- SET target_poly2 = "POLYGON((129.79369522857397 33.0837734240237,129.7933250837299 33.08244747668231,129.7891810708019 33.08286998410693,129.78980200218885 33.08413749420242,129.79152465092866 33.08433188908185,129.7916537322375 33.08412681932667,129.79369522857397 33.0837734240237))";
  
  -- その他変更点(#numberでコード内検索)
  -- other points to alter (numbered with # in the code)
    -- #0 出力する一次テーブル名
    -- #0 name of a table witch will be created
    -- #1 分析対象ポリゴンのテーブル名
    -- #1 name of a store information table
-- ★★★設定終了★★★


CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.tmp_202201_03_visitor_travel_heatmap` AS /*要変更点#0*/

WITH
union_logs AS (
  SELECT
    IF(adid IS NULL, CAST(app_user_id AS STRING),adid) AS id,
    date_time AS jst_date_time,
    EXTRACT(HOUR FROM date_time) AS jst_hour,
    EXTRACT(DATE FROM date_time) AS jst_date,
    latitude,
    longitude,
    'gps' AS log_type,
    ROW_NUMBER() OVER() AS rn
  FROM
    `beaconbank-analytics.analytics_log.gps_*`
  WHERE
     _TABLE_SUFFIX IN UNNEST(data_source)
    AND ( DATE(date_time) BETWEEN start_date2 AND end_date2)
    AND ( --九州全域
      geohash LIKE 'wvu%'
    OR geohash LIKE 'wvv%'
    OR geohash LIKE 'wvt%'
    OR geohash LIKE 'wvs%'
    OR geohash LIKE 'wyj%'
    OR geohash LIKE 'wyh%'
    )
)
/*ログを抽出*/
SELECT DISTINCT 
    -- TO_HEX(SHA256(t1.id)) AS hashed_id,
    -- t1.* except(id,rn),
    t1.* except(rn),
    -- CASE
    --   WHEN ST_DWITHIN(ST_GEOGFROMTEXT(target_poly),ST_GEOGPOINT(longitude, latitude),0) THEN 'inside_park'
    --   WHEN ST_DWITHIN(ST_GEOGFROMTEXT(target_poly2),ST_GEOGPOINT(longitude, latitude),0) THEN 'ロッテルダム特設会場'
    -- END AS visti_flag,
    age,
    gender,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    home_oaza,
    office_lat,
    office_lon,
    office_pref,
    office_city,
    office_oaza,
    repeat_flag
FROM
    union_logs AS t1
    LEFT JOIN `beaconbank-analytics.pr_db_huistenbosch.st_demogra_syoken` t2 
    ON TO_HEX(SHA256(t1.id)) = t2.id
    AND t1.jst_date BETWEEN t2.jst_date - 2 AND t2.jst_date + 2 --前後2日間のログを抽出
WHERE
    -- ST_DWITHIN(ST_GEOGFROMTEXT(target_poly),ST_GEOGPOINT(longitude, latitude),radius)
    -- AND 
    home_pref NOT IN ('長崎県','福岡県','佐賀県','大分県','宮崎県','熊本県','鹿児島県')
    AND office_pref NOT IN ('長崎県','福岡県','佐賀県','大分県','宮崎県','熊本県','鹿児島県')