-- version_202110
-- ★★★変数設定★★★
  -- 変数はなし

-- その他変更点(#numberでコード内検索)
  -- #0 出力するテーブル名
  -- #1 使用する来訪日テーブル名
-- ★★★設定終了★★★

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20211108_passage_visitor_profile` AS /*要変更点#0*/




WITH t0 AS(
  SELECT
      id,
      CASE
        WHEN jst_date <= '2021-08-31' THEN 'before'
        WHEN jst_date >= '2021-09-11' THEN 'after'
      END AS period_flag,
      COUNT(DISTINCT jst_DATE) AS cnt_date,
  FROM 
    `beaconbank-analytics.huistenbosch.20211108_passage_visitor`
  WHERE 
    jst_date NOT BETWEEN '2021-09-01' AND '2021-09-10'
    AND jst_hour BETWEEN 9 AND 20
  GROUP BY 1,2
)
, log AS(
  SELECT DISTINCT
    -- place_name,
    -- jst_date, --日毎に算出したい場合はコメントイン
    id,
    cnt_date,
    period_flag,
    CASE
      WHEN t2.Age = 10 THEN '10代'
      WHEN t2.Age = 20 THEN '20代'
      WHEN t2.Age = 30 THEN '30代'
      WHEN t2.Age = 40 THEN '40代'
      WHEN t2.Age = 50 THEN '50代'
      WHEN t2.Age = 60 THEN '60代'
      ELSE NULL
    END age,
    CASE
      WHEN t3.Gender = 'M' THEN '男性'
      WHEN t3.Gender = 'F' THEN '女性'
      ELSE NULL
    END gender,
    IF(age IS NULL AND gender IS NULL AND t4.city IS NULL AND t5.city IS NULL, 1, 0) AS exclude_flg, --age, gender, home, office どれもNULLであれば1
    t4.latitude AS home_lat,
    t4.longitude AS home_lon,
    t4.prefecture AS home_pref,
    t4.city AS home_city,
    t4.oaza AS home_oaza,
    t5.latitude AS office_lat,
    t5.longitude AS office_lon,
    t5.prefecture AS office_pref,
    t5.city AS office_city,
    t5.oaza AS office_oaza
   FROM
    (SELECT id, period_flag, cnt_date FROM t0 WHERE cnt_date < 20) t1/*要変更点#1*/
    LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_age_fac` t2 USING (id)
    LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_gender` t3 USING (id)
    LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_home_4m` t4 USING (id)
    LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_office_4m` t5 USING (id)
) 

SELECT DISTINCT
  log.*
FROM
  log
WHERE
  exclude_flg = 0 --age, gender, home, office がどれもNULLのものは除く