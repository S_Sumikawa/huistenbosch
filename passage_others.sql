SELECT DISTINCT CONCAT(id, CAST(jst_date AS STRING)) 
FROM `beaconbank-analytics.huistenbosch.20211027_passage_visitor`
WHERE jst_date NOT BETWEEN '2021-07-22' AND '2021-09-10' 

-- uuカウント用
WITH t0 AS(
SELECT
    id,
    COUNT(DISTINCT jst_DATE) AS cnt_date
FROM 
  `beaconbank-analytics.huistenbosch.20211027_passage_visitor`
WHERE 
  jst_date NOT BETWEEN '2021-07-22' AND '2021-09-10' 
GROUP BY 1
)
SELECT DISTINCT 
  CONCAT(id, CAST(jst_date AS STRING)) 
FROM 
  `beaconbank-analytics.huistenbosch.20211027_passage_visitor`
WHERE 
    jst_date NOT BETWEEN '2021-07-22' AND '2021-09-10' 
    AND id NOT IN (SELECT id FROM t0 WHERE cnt_date >=20)
-- cnt_date15 upを除く : 859人・日
-- cnt_date10 upを除く : 837人・日

-- 

-- 人数
WITH t0 AS(
  SELECT
      id,
      jst_date,
      CASE
        WHEN jst_date < '2021-07-22' THEN 'before'
        WHEN jst_date > '2021-09-10' THEN 'after'
      END AS period_flag,
      -- COUNT(DISTINCT jst_DATE) AS cnt_date,
  FROM 
    `beaconbank-analytics.huistenbosch.20211027_passage_visitor`
  WHERE 
    jst_date NOT BETWEEN '2021-07-22' AND '2021-09-10'
    AND jst_hour BETWEEN 9 AND 20
  GROUP BY 1,2
)
, t1 AS(
  SELECT DISTINCT
      id,
      jst_date,
      CASE
)
SELECT period_flag, COUNT(DISTINCT CONCAT(id, CAST(jst_date AS STRING))) AS cnt  FROM t0 WHERE cnt_date < 20


-- ユニーク人数算出用

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20211109_passage_visitor_profile_daily_unique` AS /*要変更点#0*/

SELECT DISTINCT
  * except(jst_date) ,
  COUNT(DISTINCT jst_date) OVER (PARTITION BY id) AS cnt_visit
FROM
  `beaconbank-analytics.huistenbosch.20211109_passage_visitor_profile_daily`