/*分析 宿泊判定 */
-- BeaconBankとVerasetのGPSのログを使用
-- 分析対象店舗のポリゴンを入力してログを取得
-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date DATE;

DECLARE 
 min_visit_cnt,
 max_visit_cnt INT64;

DECLARE lat1,
lat2,
lon1,
lon2 INT64;

DECLARE target_lat1,
target_lon1 FLOAT64;
DECLARE radius,
radius2,
nh_start,
nh_end INT64; 
DECLARE target_name1 STRING;
DECLARE target_poly1 STRING;

-- ★★★設定項目★★★

-- 帰宅判定をする半径
SET radius = 500;

-- 夜間の時間帯(ex.23~5)
-- nh = night_hour
SET nh_start = 23;
SET nh_end = 5;

-- 追加で宿泊判定をする対象施設ポリゴン
SET target_poly1 = 'POLYGON((129.7860946371367 33.0831477067346,129.78583714507127 33.07987547889573,129.7837342932036 33.078149419564966,129.78201767943406 33.07811345963542,129.78257557890916 33.081529587304686,129.78412053130174 33.082824085230214,129.78364846251512 33.084298351328094,129.78506466887498 33.085125367830834,129.7857513143828 33.087390634075646,129.78970626195292 33.089098059516246,129.79000403298068 33.090766637843,129.79498221291232 33.090407089473544,129.79446722878146 33.08584069727434,129.79326559914279 33.082424737051255,129.7860946371367 33.0831477067346))'
; -- 現在はハウステンボス

-- 追加で宿泊判定をする対象施設の宿泊判定範囲
-- ex. 上記のポリゴンから半径500以内に夜間にログがあれば、その施設内に宿泊したと判定する(night_before/afterのフラグが1になる)
SET radius2 = 500;


-- その他変更点(#numberでコード内検索)
-- #0 出力する一次テーブル名

-- ★★★設定終了★★★

-- CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210519_huistenbosch_travel_behavior` AS /*要変更点#0*/

WITH stay_judgement_log1 AS(
    SELECT
        id,
        CASE
            WHEN visit_flag = '1day before' THEN DATE_ADD(jst_date, INTERVAL 1 day)
            WHEN visit_flag = '1day after' THEN DATE_SUB(jst_date, INTERVAL 1 day)
            ELSE jst_date
        END AS  visit_date,
        longitude,
        latitude,
        visit_flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,

        -- night_before:前泊判定
        -- night_after:後泊判定
        --      0:非宿泊（帰宅）
        --      1:追加した施設内に宿泊（ハウステンボス）
        --      2:宿泊
        --      null:夜間ログなし

        CASE
            WHEN -- 居住地周辺のログの場合
                (   (hour <= nh_end AND visit_flag IN ('visit day')) 
                    OR (hour >= nh_start AND visit_flag IN ('1day before'))
                    )
                AND ST_DWITHIN(ST_GeogPoint(home_lon, home_lat),ST_GeogPoint(longitude,latitude),radius)
                THEN 0
            WHEN 
                (   (hour <= nh_end AND visit_flag IN ('visit day')) 
                    OR (hour >= nh_start AND visit_flag IN ('1day before'))
                    )
                THEN 2
        END AS night_before,
        CASE
            WHEN -- 居住地周辺のログの場合
                (   (hour <= nh_end AND visit_flag IN ('1day after')) 
                    OR (hour >= nh_start AND visit_flag IN ('visit day'))
                    )
                AND ST_DWITHIN(ST_GeogPoint(home_lon, home_lat),ST_GeogPoint(longitude,latitude),radius)
                THEN 0
            WHEN 
                (   (hour <= nh_end AND visit_flag IN ('1day after')) 
                    OR (hour >= nh_start AND visit_flag IN ('visit day'))
                    )
                THEN 2
        END AS night_after
    FROM
        `labs-analytics-298709.works_sumikawa.20210518_huistenbosch_sameday_behavior`
    WHERE
        (hour <= nh_end 
            AND visit_flag IN ('visit day','1day after')
        )
        OR 
        (hour >= nh_start 
            AND visit_flag IN ('1day before','visit day')
        )
),
stay_judgement_log2 AS(
    SELECT
        id,
        visit_date,
        visit_flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        CASE
            WHEN -- ハウステンボス周辺のログの場合
                night_before = 2
                AND ST_DWITHIN(ST_GEOGFROMTEXT(target_poly1),ST_GeogPoint(longitude,latitude),radius2)
                THEN 1
            ELSE night_before
        END AS night_before,
        CASE
            WHEN -- ハウステンボス周辺のログの場合
                night_after = 2
                AND ST_DWITHIN(ST_GEOGFROMTEXT(target_poly1),ST_GeogPoint(longitude,latitude),radius2)
                THEN 1
            ELSE night_after
        END AS night_after
    FROM
        stay_judgement_log1
),
stay_judgement_id AS( -- 個々人について宿泊判定
    SELECT
        id,
        visit_date,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        MIN(night_before) AS night_before,
        MIN(night_after) AS night_after,
    FROM
        stay_judgement_log2 AS log
    GROUP BY
        1,2,3,4,5,6,7,8,9,10
)
SELECT
    *
FROM
    stay_judgement_id