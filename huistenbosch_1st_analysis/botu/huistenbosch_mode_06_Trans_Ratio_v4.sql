DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date DATE;

DECLARE target_poly1 STRING;

-- ★★★設定項目★★★
-- ターゲット期間1
SET start_date1 = '2020-01-01';
SET end_date1 = '2020-02-29';

-- ターゲット期間2
SET start_date2 = '2020-03-01';
SET end_date2 = '2020-04-30';

-- 追加で宿泊判定をする対象施設ポリゴン
SET target_poly1 = 'POLYGON((129.79502102873826 33.090410898295374,129.78982827208543 33.090662582297455,129.78948494933152 33.088577178820074,129.78557965300584 33.08746254633412,129.78515049956346 33.084801753603784,129.78382012389207 33.08397473405779,129.78248974822068 33.0806665781028,129.78222663647682 33.078276836686236,129.78372867352516 33.078276836686236,129.78619630581886 33.08012875050239,129.78632505185158 33.083113306246396,129.7932153733119 33.08230508588857,129.79493198708144 33.0874829176132,129.79502102873826 33.090410898295374))'
; -- 現在はハウステンボス

CREATE OR REPLACE TABLE
`labs-analytics-298709.works_sumikawa.20210526_huistenbosch_06_Trans_Ratio_v5`
AS

WITH
  log AS (
    SELECT *
    FROM `labs-analytics-298709.works_sumikawa.20210526_huistenbosch_04_log_visitor_v5`
    WHERE date BETWEEN start_date1 AND end_date2
    -- FORMAT_DATE("%Y%m%d", start_date1)
    --     AND FORMAT_DATE("%Y%m%d", end_date2) 
        AND stay_minit >= 10
    --   AND NOT ST_COVERS(ST_GEOGFROMTEXT(target_poly1), ST_GEOGPOINT(home_lon, home_lat))
  ),
  UP AS (
    SELECT
      id AS id,
      home_lat,
      home_lon,
      home_pref,
      home_city
--       CAST (
--         floor(home_lat*3/2)*10000000
--        +floor(home_lon-100)*100000
--        +floor(mod(cast(home_lat as numeric)*60,40)/5)*10000
--        +floor(mod(cast(home_lon as numeric)-100,1)*60/7.5)*1000
--        +floor(mod(mod(cast(home_lat as numeric)*60,40),5)*60/30)*100
--        +floor(mod(mod((cast(home_lon as numeric)-100),1)*60,7.5)*60/45)*10
--        +((floor(mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10/0.25)*2)
--        +(floor(mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10/0.375)+1))*1
--       AS string) AS meshcode500,
--       CAST (
--         floor(home_lat*3/2)*100000000
--        +floor(home_lon-100)*1000000
--        +floor(mod(cast(home_lat as numeric)*60,40)/5)*100000
--        +floor(mod(cast(home_lon as numeric)-100,1)*60/7.5)*10000
--        +floor(mod(mod(cast(home_lat as numeric)*60,40),5)*60/30)*1000
--        +floor(mod(mod((cast(home_lon as numeric)-100),1)*60,7.5)*60/45)*100
--        +((floor(mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10/0.25)*2)
--        +(floor(mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10/0.375)+1))*10
--        +((floor(mod((mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10)*10,2.5)/10/0.125)*2)
--        +(floor(mod((mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10)*10,3.75)/10/0.1875)+1))*1
--       AS string) AS meshcode250,
--       UDF.geoToH3(home_lat, home_lon,11) AS geoH3
    FROM `beaconbank-analytics.user_profile.UP_v2_Home20201201`
  ),
  trans_sum AS (
    SELECT DISTINCT
      date,
      id,
      SUM(cnt_ship) AS cnt_ship,
      SUM(cnt_train_p) AS cnt_train_p,
      SUM(cnt_train) AS cnt_train,
      SUM(cnt_car) AS cnt_car,
      SUM(cnt_walk) AS cnt_walk,
      SUM(cnt_total) AS cnt_total,
--       home_pref,
--       home_city,
--       meshcode500_log,
--       cnt_log_trans,
--       cnt_log_total,
--       cnt_log_trans / cnt_log_total AS trans_ratio
    FROM (
      SELECT 
        date,
        a.id,
        SUM(CASE WHEN mode = 'Ship' THEN 1 ELSE 0 END) AS cnt_ship,
        SUM(CASE WHEN mode = 'Train_p' THEN 1 ELSE 0 END) AS cnt_train_p,
        SUM(CASE WHEN mode = 'Train' THEN 1 ELSE 0 END) AS cnt_train,
        SUM(CASE WHEN mode = 'Car' THEN 1 ELSE 0 END) AS cnt_car,
        SUM(CASE WHEN mode = 'Walk' THEN 1 ELSE 0 END) AS cnt_walk,
--         a.home_pref,
--         a.home_city,
--         a.home_lat,
--         a.home_lon,
--         CAST (
--           floor(a.home_lat*3/2)*10000000 
--          +floor(a.home_lon-100)*100000　
--          +floor(mod(cast(a.home_lat as numeric)*60,40)/5)*10000
--          +floor(mod(cast(a.home_lon as numeric)-100,1)*60/7.5)*1000
--          +floor(mod(mod(cast(a.home_lat as numeric)*60,40),5)*60/30)*100
--          +floor(mod(mod((cast(a.home_lon as numeric)-100),1)*60,7.5)*60/45)*10
--          +((floor(mod((mod(mod(cast(a.home_lat as numeric)*60,40),5)*10),5)/10/0.25)*2) 
--          +(floor(mod((mod(mod(cast(a.home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10/0.375)+1))*1
--         AS string) AS meshcode500_log,
        COUNT(a.id)  AS cnt_total,--OVER (PARTITION BY date, mode) /
--         COUNT(a.id) OVER (PARTITION BY date) AS cnt_log_total
      FROM log a
    --   INNER JOIN UP ON UP.id = a.id
      GROUP BY
        a.date,
        a.id,
        a.mode
    )
    GROUP BY
      date,
      id
  ),
trans_ratio AS (
    SELECT DISTINCT
      date,
      id,
      cnt_ship,
      cnt_train_p,
      cnt_train,
      cnt_car,
      cnt_walk,
      cnt_total,
      cnt_ship / cnt_total AS ratio_ship,
      cnt_train / cnt_total AS ratio_train,
      cnt_car / cnt_total AS ratio_car,
      CASE 
        WHEN cnt_ship / cnt_total > 0.0 THEN 'Ship'
        WHEN cnt_train_p / cnt_total > 0.0 THEN 'Train'
        WHEN cnt_train / cnt_total >= 0.06 THEN 'Train'
        WHEN cnt_car / cnt_total >= 0.1 THEN 'Car'
        -- ELSE 'Walk' 
      END AS mode
      FROM trans_sum
  )

SELECT *
FROM trans_ratio