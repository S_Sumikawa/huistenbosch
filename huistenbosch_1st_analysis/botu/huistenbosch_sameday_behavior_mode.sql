CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210607_huistenbosch_sameday_behavior_mode` AS
SELECT *
FROM (
SELECT
  DATE,
    id,
    jst_time,
    latitude,
    longitude,
    division_id,
    event_visit,
    cut,
    dif_second_used,
    dif_dist_used,
    velocity,
    stay_second,
    stay_minit,
    stay_hour,
    stay_hour_int,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    meshcode500,
    meshcode250,
    geoH3,
    mode AS log_mode
FROM
`beaconbank-analytics.huistenbosch.20210607_huistenbosch_04_log_visitor` a
)
JOIN `beaconbank-analytics.huistenbosch.20210607_huistenbosch_06_Trans_Ratio` b
USING (id, DATE)
WHERE DATE BETWEEN "2018-01-01" AND  "2022-12-31"