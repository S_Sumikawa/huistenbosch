WITH log_cnt AS(
    SELECT
        id,
        jst_date,
        jst_date - 180 AS before_half_year,
        cnt_log
--         COUNT(*) AS cnt_log,
    FROM
        `labs-analytics-298709.works_sumikawa.20210603_huistenboshc_repeat_test`
--     GROUP BY 1,2,3
),
repeat AS(
    SELECT
        id,
        jst_date,
        before_half_year,
        LAG(jst_date) OVER(PARTITION BY id ORDER BY jst_date) AS last_visited,
        CASE
            WHEN before_half_year <= LAG(jst_date)
            OVER(PARTITION BY id ORDER BY jst_date)
                THEN 'repeat'
            WHEN '2019-07-01' <= LAG(jst_date)
            OVER(PARTITION BY id ORDER BY jst_date)
                THEN 'come_back'
            ELSE 'new'
        END AS repeat_flag,
    FROM
        (SELECT
            *
        FROM
            log_cnt
        WHERE
            cnt_log >= 2
        )
)
SELECT * FROM repeat ORDER BY id