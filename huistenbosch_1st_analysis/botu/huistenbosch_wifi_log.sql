CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_log_for_ML` AS /*要変更点#0*/

WITH
  -- logにoui情報負荷
  LOG_TEMP AS (
  SELECT
    device_id,
    mac_address,
    rssi,
    oui,
    is_ap_connected,
    TIMESTAMP(DATETIME(first_detected, 'Asia/Tokyo')) AS first_detected_jst,
    TIMESTAMP(DATETIME(last_detected, 'Asia/Tokyo')) AS last_detected_jst,
    CAST(SUBSTR(FORMAT_TIMESTAMP('%Y%m%d%H%M', TIMESTAMP(DATETIME(first_detected, 'Asia/Tokyo'))), 0, 8) AS INT64) AS date,
    CAST(SUBSTR(FORMAT_TIMESTAMP('%Y%m%d%H%M', TIMESTAMP(DATETIME(last_detected, 'Asia/Tokyo'))), 9, 2) AS INT64) AS hour,
    FORMAT_TIMESTAMP('%a', TIMESTAMP(DATETIME(first_detected, 'Asia/Tokyo')))　AS　day_of_week
  FROM
    `wifi_log.wifilog_v3`
  WHERE
    DATE(last_detected,'Asia/Tokyo') BETWEEN "2021-04-01"
    AND "2021-05-31"
    AND device_id IN (
    'RP_1607046805302567888',
    'RP_1607046803655876876',
    'RP_1611307908901850490',
    'RP_1607053262077342491'
    )
  ORDER BY
    last_detected,
    mac_address ),
    
  -- logの絞り込み
  MOBILE_LOG AS (
  SELECT
    log.mac_address,
    log.rssi,
    log.device_id,
    log.is_ap_connected,
    log.first_detected_jst,
    log.last_detected_jst,
    CAST(SUBSTR(FORMAT_TIMESTAMP('%Y%m%d%H%M', TIMESTAMP(CAST(log.last_detected_jst AS String))), 0, 8) AS INT64) AS DATETIME,
    CAST(SUBSTR(FORMAT_TIMESTAMP('%Y%m%d%H%M', TIMESTAMP(CAST(log.last_detected_jst AS String))), 9, 2) AS INT64) AS hour,
    oui.Corporation as corporation
  FROM
    LOG_TEMP log
  LEFT JOIN
    wifi_log.oui oui
  ON
    log.oui = oui.Oui
  WHERE
    log.mac_address NOT LIKE "2E:30:5B%" AND
    (oui.Corporation IS NULL
      OR oui.Corporation LIKE "Apple%"
      OR oui.Corporation LIKE "Sony%"
      OR oui.Corporation LIKE "ASUS%"
      OR oui.Corporation LIKE "FUJITSU%"
      OR oui.Corporation LIKE "%OPPO%"
      OR oui.Corporation LIKE "HTC%"
      OR oui.Corporation LIKE "Huawei%"
      OR oui.Corporation LIKE "Kyocera%"
      OR oui.Corporation LIKE "LG%"
      OR oui.Corporation LIKE "Motorola%"
      OR oui.Corporation LIKE "Samsung%"
      OR oui.Corporation LIKE "SHARP%"
      OR oui.Corporation LIKE "zte%"
      OR oui.Corporation LIKE "Xiaomi%"
      OR oui.Corporation LIKE "Covia%"
      OR oui.Corporation LIKE "Panasonic%"
      OR oui.Corporation LIKE "Lenovo Mobile%"
      OR oui.Corporation LIKE "OnePlus%"
      OR oui.Corporation LIKE "Essential%"
      OR oui.Corporation LIKE "TCT%"
      OR oui.Corporation LIKE "vivo%"
      OR oui.Corporation LIKE "Google%")
    AND log.Rssi < -1　
    AND log.last_detected_jst >= "2021-04-01 00:00:00"
    AND log.last_detected_jst < "2021-06-01 00:00:00"
    AND log.rssi > -54
  GROUP BY
    log.mac_address,
    log.rssi,
    log.is_ap_connected,
    log.first_detected_jst,
    log.last_detected_jst,
    log.device_id,
    oui.corporation)
  
  
SELECT 
  device_id, 
  datetime, 
  hour,
  COUNT(DISTINCT mac_address) AS cnt,
  CEIL(( COUNT(DISTINCT mac_address ) /61.4 * 100 ) / 94 * 100 )  as cnt_compensated
  -- wifiのon率が 61.4%
  -- スマホ保持率 94％
  -- CEIL(X):X 以上で最小の整数値を返す
FROM 
  MOBILE_LOG 
GROUP BY
  device_id, datetime, hour
ORDER BY
  device_id, datetime, hour
  