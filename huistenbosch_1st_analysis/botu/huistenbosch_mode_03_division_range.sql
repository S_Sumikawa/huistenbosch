-- DECLARE x STRING DEFAULT '20200930'; 
-- LOOP
-- INSERT INTO user_profile.UP_DNA_MaaS_167_daily

DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date DATE;

-- ★★★設定項目★★★
-- ターゲット期間1
SET start_date1 = '2020-01-01';
SET end_date1 = '2020-02-29';

-- ターゲット期間2
SET start_date2 = '2020-03-01';
SET end_date2 = '2020-04-30';

CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210524_huistembosch_03_division_range` 
PARTITION BY date
CLUSTER BY id
AS

WITH
  division_range AS (
    SELECT
      date,
      id,
--       lag_adid,
      jst_time,
--       lag_time,
      latitude,
--       lag_lat,
      longitude,
--       lag_lon,
      event_visit,
--       lag_event_visit,
      DATETIME_DIFF(jst_time, lag_time, SECOND) AS dif_second,
      ST_DISTANCE(ST_GEOGPOINT(longitude, latitude), ST_GEOGPOINT(lag_lon, lag_lat)) AS dif_dist,
      CASE
        WHEN (CASE WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1 ELSE 0 END) = 1 THEN 0
        ELSE DATETIME_DIFF(jst_time, lag_time, SECOND) 
      END AS dif_second_used,
      CASE
        WHEN (CASE WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1 ELSE 0 END) = 1 THEN 0
        ELSE ST_DISTANCE(ST_GEOGPOINT(longitude, latitude), ST_GEOGPOINT(lag_lon, lag_lat)) 
      END AS dif_dist_used,
---- 分割定義 ①ユーザが異なる/後続のデータが無い　②トリップでエリアに入っている/エリアから出ている　③同じユーザでも前後のログ間隔が10分以上ある
---- この場合はcut = 1 でデータは使用されない dif_second_used, dif_dist_used は使用可能な場合のみ トリップ所要時間/ トリップ距離が入る
      CASE
        WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1
        ELSE 0 END AS cut,
      SUM(CASE
        WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1
        ELSE 0 
      END) OVER(ORDER BY id, jst_time) AS division_id
--     FROM log_sameday
    FROM `labs-analytics-298709.works_sumikawa.20210524_huistembosch_log_sameday`
  --     WHERE event_visit = 'in'
    WHERE date BETWEEN DATE(start_date1) AND DATE(end_date2)
    GROUP BY
      date,
      id,
      lag_adid,
      lag_event_visit,
      lag_time,
      lag_lat,
      lag_lon,
      jst_time,
      latitude,
      longitude,
      event_visit,
      dif_second,
      dif_dist
  ),

  --滞在時間算出
  stay_time AS (
    SELECT
      division_id AS division_id_stay,
      SUM(dif_second_used ) AS stay_second,
      SUM(dif_second_used ) / 60 AS stay_minit,
      (SUM(dif_second_used ) / 60 ) /60 AS stay_hour,
      CAST( (SUM(dif_second_used ) / 60 ) /60 AS int64) AS stay_hour_int
    FROM division_range
    WHERE dif_second IS NOT NULL
    GROUP BY division_id 
            ),
            
            UP AS (
                SELECT
                id AS id,
                home_lat,
                home_lon,
                home_pref,
                home_city,
                CAST (
                    floor(home_lat*3/2)*10000000 --1次メッシュ緯度 （80km四方）
                +floor(home_lon-100)*100000　 --1次メッシュ経度
                +floor(mod(cast(home_lat as numeric)*60,40)/5)*10000 --2次メッシュ緯度判定 （10km四方）
                +floor(mod(cast(home_lon as numeric)-100,1)*60/7.5)*1000 --2次メッシュ経度判定
                +floor(mod(mod(cast(home_lat as numeric)*60,40),5)*60/30)*100 --3次メッシュ緯度判定 （1km四方）
                +floor(mod(mod((cast(home_lon as numeric)-100),1)*60,7.5)*60/45)*10 --3次メッシュ経度判定
                +((floor(mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10/0.25)*2) --1/2メッシュ緯度判定 （500m四方）
                +(floor(mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10/0.375)+1))*1 --1/2メッシュ経度判定
            --        +((floor(mod((mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10)*10,2.5)/10/0.125)*2) --1/4メッシュ緯度判定 （250m四方）
            --        +(floor(mod((mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10)*10,3.75)/10/0.1875)+1))*1 --1/4メッシュ経度判定
            --        +((floor(mod((mod((mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10)*10,2.5)/10)*10,1.25)/10/0.0625)*2) --1/8メッシュ緯度判定 --（125m四方）
            --        +(floor(mod((mod((mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10)*10,3.75)/10)*10,1.875)/10/0.09375)+1))*1 --1/8メッシュ経度判定
                AS string) AS meshcode500,
                CAST (
                    floor(home_lat*3/2)*100000000 --1次メッシュ緯度 （80km四方）
                +floor(home_lon-100)*1000000　 --1次メッシュ経度
                +floor(mod(cast(home_lat as numeric)*60,40)/5)*100000 --2次メッシュ緯度判定 （10km四方）
                +floor(mod(cast(home_lon as numeric)-100,1)*60/7.5)*10000 --2次メッシュ経度判定
                +floor(mod(mod(cast(home_lat as numeric)*60,40),5)*60/30)*1000 --3次メッシュ緯度判定 （1km四方）
                +floor(mod(mod((cast(home_lon as numeric)-100),1)*60,7.5)*60/45)*100 --3次メッシュ経度判定
                +((floor(mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10/0.25)*2) --1/2メッシュ緯度判定 （500m四方）
                +(floor(mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10/0.375)+1))*10 --1/2メッシュ経度判定
                +((floor(mod((mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10)*10,2.5)/10/0.125)*2) --1/4メッシュ緯度判定 （250m四方）
                +(floor(mod((mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10)*10,3.75)/10/0.1875)+1))*1 --1/4メッシュ経度判定
            --        +((floor(mod((mod((mod((mod(mod(cast(home_lat as numeric)*60,40),5)*10),5)/10)*10,2.5)/10)*10,1.25)/10/0.0625)*2) --1/8メッシュ緯度判定 --（125m四方）
            --        +(floor(mod((mod((mod((mod(mod(cast(home_lon as numeric)-100,1)*60,7.5)*10),7.5)/10)*10,3.75)/10)*10,1.875)/10/0.09375)+1))*1 --1/8メッシュ経度判定
                AS string) AS meshcode250,
                UDF.geoToH3(home_lat, home_lon,11) AS geoH3
                FROM `beaconbank-analytics.user_profile.UP_v2_Home20201201`
                WHERE id_type = 'id'
            ),

            ---- 富山駅周辺の鉄道ラインデータ
            -- TL_road AS ( -- 路面電車
            --     SELECT *
            --     FROM `beaconbank-analytics.place_master.TrainLines`
            --     WHERE Company IN ('九州旅客鉄道') AND LineName IN ('大村線')
            -- ),
            TL AS (
                SELECT *
                FROM `beaconbank-analytics.place_master.TrainLines`
                WHERE Company IN ('九州旅客鉄道') AND LineName IN ('大村線')
            ),

            log_visitor AS (
                SELECT
                c.date,
                c.id,
            --       lag_adid,
                jst_time,
            --       lag_time,
            --       dif_second,
                c.latitude,
                c.longitude,
                division_id,
            --       lag_lat,
            --       lag_lon,
            --       dif_dist,
                event_visit,
            --       lag_event_visit,
                cut,
                dif_second_used,
                dif_dist_used,
            --       division_id,
                (dif_dist/1000) / (dif_second/3600) AS velocity,
                CASE
                    -- WHEN ST_DWITHIN(ST_GEOGFROMGEOJSON(TL_road.geom), ST_GEOGPOINT(c.longitude, c.latitude), 2) THEN 'Train'
                    WHEN ST_DWITHIN(ST_GEOGFROMGEOJSON(TL.geom), ST_GEOGPOINT(c.longitude, c.latitude), 20) THEN 'Train'
                    WHEN (dif_dist/1000) / (dif_second/3600) >= 5 THEN 'Car'
                    ELSE 'Walk'
                END AS transportation,
                stay_second,
                stay_minit,
                stay_hour,
                stay_hour_int,
            --       Gender,
                home_lat,
                home_lon,
                home_pref,
                home_city,
                meshcode500,
                meshcode250,
                geoH3,
            --       home_accuracy,
            --       home_oaza,
            --       home_koaza,
            --       office_lat,
            --       office_lon,
            --       office_accuracy,
            --       office_pref,
            --       office_city,
            --       office_oaza,
            --       office_koaza,
                --富山駅前のポリゴンと居住地の比較
            --       CASE
            --         WHEN ST_COVERS(ST_GEOGFROMTEXT('POLYGON((137.20891	36.712851,137.203074 36.699088,137.199812	36.684291,137.230883 36.683396,137.236204	36.709617,137.20891	36.712851))')
            --               , ST_GEOGPOINT(home_lon, home_lat)) THEN 'エリア内居住者' 
            --       END AS home_user,
                --富山駅前のポリゴンと勤務地の比較
            --       CASE
            --         WHEN ST_COVERS(ST_GEOGFROMTEXT('POLYGON((137.20891	36.712851,137.203074 36.699088,137.199812	36.684291,137.230883 36.683396,137.236204	36.709617,137.20891	36.712851))')
            --               , ST_GEOGPOINT(office_lon, office_lat)) THEN 'エリア内勤務者'
            --       END AS office_user
                FROM (
                SELECT *
                FROM division_range a
                INNER JOIN stay_time b ON a.division_id = b.division_id_stay
                ORDER BY
                    id,
                    jst_time
                ) c
                -- , TL_road
                , TL
                LEFT JOIN UP d ON c.id = d.id
                WHERE dif_second > 1
  )

SELECT *
FROM division_range

-- SELECT
--   id
--   , date
--   , jst_time
--   , transportation
--   , velocity
--   , division_id
--   , stay_second
--   , stay_minit
--   , stay_hour
--   , stay_hour_int
--   , latitude
--   , longitude
--   , home_lat
--   , home_lon
--   , home_pref
--   , home_city
--   , meshcode500
--   , meshcode250
--   , geoH3
-- --   , division_id
--   , event_visit
--   , COUNT(id) AS cnt_log
-- FROM log_visitor
-- WHERE cut = 0
-- GROUP BY
--   id
--   , date
--   , jst_time
--   , transportation
--   , velocity
--   , division_id
--   , stay_second
--   , stay_minit
--   , stay_hour
--   , stay_hour_int
--   , latitude
--   , longitude
--   , home_lat
--   , home_lon
--   , home_pref
--   , home_city
--   , meshcode500
--   , meshcode250
--   , geoH3
--   , latitude
--   , longitude
-- --   , division_id
--   , event_visit
