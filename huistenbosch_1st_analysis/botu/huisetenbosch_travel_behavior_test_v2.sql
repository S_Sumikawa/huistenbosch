/*分析 宿泊判定 */
-- BeaconBankとVerasetのGPSのログを使用
-- 分析対象店舗のポリゴンを入力してログを取得
-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date DATE;

DECLARE 
 min_visit_cnt,
 max_visit_cnt INT64;

DECLARE lat1,
lat2,
lon1,
lon2 INT64;

DECLARE target_lat1,
target_lon1 FLOAT64;
DECLARE radius,
radius2,
nh_start,
nh_end INT64; 
DECLARE target_name1 STRING;
DECLARE target_poly1 STRING;

-- ★★★設定項目★★★

-- 帰宅判定をする半径
SET radius = 500;

-- 夜間の時間帯(ex.23~5)
-- nh = night_hour
SET nh_start = 23;
SET nh_end = 5;

-- 追加で宿泊判定をする対象施設ポリゴン
SET target_poly1 = 'POLYGON((129.79502102873826 33.090410898295374,129.78982827208543 33.090662582297455,129.78948494933152 33.088577178820074,129.78557965300584 33.08746254633412,129.78515049956346 33.084801753603784,129.78382012389207 33.08397473405779,129.78248974822068 33.0806665781028,129.78222663647682 33.078276836686236,129.78372867352516 33.078276836686236,129.78619630581886 33.08012875050239,129.78632505185158 33.083113306246396,129.7932153733119 33.08230508588857,129.79493198708144 33.0874829176132,129.79502102873826 33.090410898295374))'
; -- 現在はハウステンボス

-- 追加で宿泊判定をする対象施設の宿泊判定範囲
-- ex. 上記のポリゴンから半径500以内に夜間にログがあれば、その施設内に宿泊したと判定する(night_before/afterのフラグが1になる)
SET radius2 = 500;


-- その他変更点(#numberでコード内検索)
-- #0 出力する一次テーブル名

-- ★★★設定終了★★★

-- CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210519_huistenbosch_travel_behavior` AS /*要変更点#0*/

WITH stay_judgement_log1 AS( -- 帰宅判定
    SELECT
        id,
        CASE
            WHEN visit_flag = '1day before' THEN DATE_ADD(jst_date, INTERVAL 1 day)
            WHEN visit_flag = '1day after' THEN DATE_SUB(jst_date, INTERVAL 1 day)
            ELSE jst_date
        END AS  visit_date,
        longitude,
        latitude,
        visit_flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,

        -- night_before:前泊判定
        -- night_after:後泊判定
        --      0:非宿泊（帰宅）
        --      1:追加した施設内に宿泊（ハウステンボス）
        --      2:宿泊
        --      null:夜間ログなし

        CASE
            WHEN -- 居住地周辺のログの場合
                (   (hour <= nh_end AND visit_flag IN ('visit day')) 
                    OR (hour >= nh_start AND visit_flag IN ('1day before'))
                    )
                AND ST_DWITHIN(ST_GeogPoint(home_lon, home_lat),ST_GeogPoint(longitude,latitude),radius)
                THEN 0
            WHEN 
                (   (hour <= nh_end AND visit_flag IN ('visit day')) 
                    OR (hour >= nh_start AND visit_flag IN ('1day before'))
                    )
                THEN 2
        END AS night_before,
        CASE
            WHEN -- 居住地周辺のログの場合
                (   (hour <= nh_end AND visit_flag IN ('1day after')) 
                    OR (hour >= nh_start AND visit_flag IN ('visit day'))
                    )
                AND ST_DWITHIN(ST_GeogPoint(home_lon, home_lat),ST_GeogPoint(longitude,latitude),radius)
                THEN 0
            WHEN 
                (   (hour <= nh_end AND visit_flag IN ('1day after')) 
                    OR (hour >= nh_start AND visit_flag IN ('visit day'))
                    )
                THEN 2
        END AS night_after
    FROM
        `labs-analytics-298709.works_sumikawa.20210520_huistenbosch_sameday_behavior_v3`
    WHERE
        (hour <= nh_end 
            AND visit_flag IN ('visit day','1day after')
        )
        OR 
        (hour >= nh_start 
            AND visit_flag IN ('1day before','visit day')
        )
),
stay_judgement_log2 AS( -- HTB内宿泊判定
    SELECT
        id,
        visit_date,
        visit_flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        CASE
            WHEN -- ハウステンボス周辺のログの場合
                night_before = 2
                AND ST_DWITHIN(ST_GEOGFROMTEXT(target_poly1),ST_GeogPoint(longitude,latitude),radius2)
                THEN 1
            ELSE night_before
        END AS night_before,
        CASE
            WHEN -- ハウステンボス周辺のログの場合
                night_after = 2
                AND ST_DWITHIN(ST_GEOGFROMTEXT(target_poly1),ST_GeogPoint(longitude,latitude),radius2)
                THEN 1
            ELSE night_after
        END AS night_after
    FROM
        stay_judgement_log1
),
stay_judgement_id AS( -- 個々人について宿泊判定
    SELECT
        id,
        visit_date,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        MIN(night_before) AS night_before,
        MIN(night_after) AS night_after,
    FROM
        stay_judgement_log2 AS log
    GROUP BY
        1,2,3,4,5,6,7,8,9,10
)
SELECT
    *
FROM
    stay_judgement_id