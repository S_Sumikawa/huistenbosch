/*分析 来訪頻度を指定してログを抽出_直接入力 */
-- BeaconBankとVerasetのGPSのログを使用
-- 分析対象店舗のポリゴンを入力してログを取得
-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date DATE;

DECLARE 
 min_visit_cnt,
 max_visit_cnt,
 min_log_cnt INT64;

DECLARE lat1,
lat2,
lon1,
lon2 INT64;

DECLARE target_lat1,
target_lon1 FLOAT64;
DECLARE radius1,
opening_time1,
closing_time1 INT64;
DECLARE target_name1 STRING;
DECLARE target_poly1 STRING;

DECLARE target_lat2,
target_lon2 FLOAT64;
DECLARE radius2,
opening_time2,
closing_time2 INT64;
DECLARE target_name2 STRING;
DECLARE target_poly2 STRING;

-- DECLARE target_lat3,
-- target_lon3 FLOAT64;
-- DECLARE radius3,
-- opening_time3,
-- closing_time3 INT64;
-- DECLARE target_name3 STRING;
-- DECLARE target_poly3 STRING;

-- DECLARE target_lat4,
-- target_lon4 FLOAT64;
-- DECLARE radius4,
-- opening_time4,
-- closing_time4 INT64;
-- DECLARE target_name4 STRING;
-- DECLARE target_poly4 STRING;

-- ★★★設定項目★★★
-- ターゲット期間1
SET start_date1 = '2020-01-01';
SET end_date1 = '2020-02-29';

-- ターゲット期間2
SET start_date2 = '2020-03-01';
SET end_date2 = '2020-04-30';

-- 前後比較の区切りの日付(flag=before,afterの区切り)
-- 日付 = separate_dateの場合はbeforeのフラグがつく
SET separate_date = '2020-02-29';

-- ログ検索範囲設定用
SET lat1 = 23;
SET lat2 = 46;
SET lon1 = 123;
SET lon2 = 148;

-- 来訪判定をするログ数の下限（=2：来訪ログが2つ以上ない場合は削除）
SET
  min_log_cnt = 2;

-- 来訪日数の下限（=3：来訪日数が3日以上のログを抽出）
SET
  min_visit_cnt = 1;
-- 来訪日数の上限（=20：来訪日数が20日以下のログを抽出）
SET
  max_visit_cnt = 60;

-- 対象店舗/エリア1のポリゴン・抽出半径・営業時間情報
-- SET target_lat1 = 36.56183463;
-- SET target_lon1 = 139.8629231;
-- SET radius1 = 80;
SET opening_time1 = 9;
SET closing_time1 = 22;
SET target_name1 = 'ハウステンボス';
SET target_poly1 = 'POLYGON((129.79502102873826 33.090410898295374,129.78982827208543 33.090662582297455,129.78948494933152 33.088577178820074,129.78557965300584 33.08746254633412,129.78515049956346 33.084801753603784,129.78382012389207 33.08397473405779,129.78248974822068 33.0806665781028,129.78222663647682 33.078276836686236,129.78372867352516 33.078276836686236,129.78619630581886 33.08012875050239,129.78632505185158 33.083113306246396,129.7932153733119 33.08230508588857,129.79493198708144 33.0874829176132,129.79502102873826 33.090410898295374))'
;
-- 対象店舗/エリア2のポリゴン・抽出半径・営業時間情報
-- SET target_lat2 = 36.56081217;
-- SET target_lon2 = 139.8854019;
-- SET radius2 = 80;
-- SET opening_time2 = 8;
-- SET closing_time2 = 21;
-- SET target_name2 = '店名2';
-- SET target_poly2 = 'POLYGON((139.759687 35.674909,139.762567 35.67305,139.763768 35.673399,139.765109 35.675342,139.764369 35.67549,139.762395 35.67413,139.762272 35.674026,139.762814 35.675882,139.76365 35.675682,139.764509 35.676405,139.765174 35.677381,139.76591 35.679528,139.765374 35.680242,139.766217 35.682579,139.767162 35.683085,139.768848 35.687287,139.767861 35.68834,139.764256 35.689107,139.761016 35.689403,139.761509 35.687224,139.760243 35.684628,139.759814 35.68315,139.75578 35.676911,139.759687 35.674909))'
-- ;

-- -- 対象店舗/エリア3の緯度経度・抽出半径・営業時間情報
-- SET target_lat3 = 36.56081317;
-- SET target_lon3 = 139.8854019;
-- SET radius3 = 80;
-- SET opening_time3 = 8;
-- SET closing_time3 = 21;
-- SET target_name3 = '店名3';
-- SET target_poly3 = 'POLYGON(())'
-- ;

-- -- 対象店舗/エリア4の緯度経度・抽出半径・営業時間情報
-- SET target_lat4 = 36.56081417;
-- SET target_lon4 = 139.8854019;
-- SET radius4 = 80;
-- SET opening_time4 = 8;
-- SET closing_time4 = 21;
-- SET target_name4 = '店名4';
-- SET target_poly4 = 'POLYGON(())'
-- ;

-- その他変更点(#numberでコード内検索)
-- #0 出力する一次テーブル名
-- #1 beacon_idの指定（beaconログを使用する場合）
-- #2 分析対象店舗テーブル名
-- #3 外部データの有無
-- 　対象店舗を増やす場合はtarget_lat3などで検索して、対象箇所をコメントインする

-- ★★★設定終了★★★

CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210526_huistenbosch_profile_v5` AS /*要変更点#0*/(

WITH union_logs AS(
    /*BeaconBankのロケーションログを抽出*/
    SELECT
        -- adid,
        IF (adid IS NULL,CAST(app_user_id AS STRING),adid) AS id,
        DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo') AS jst_time,
        --detected_timeはUTCなのでJSTに変換
        longitude,
        latitude
    FROM
        `beaconbank-analytics.daily_copy.BB_locationlog*`
    WHERE
        (
            _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date1)
            AND FORMAT_DATE("%Y%m%d", end_date1)
            OR _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date2)
            AND FORMAT_DATE("%Y%m%d", end_date2)
        )
        AND latitude BETWEEN lat1
        AND lat2
        AND longitude BETWEEN lon1
        AND lon2
    -- UNION ALL　/*要変更点#3_1 Predecioのデータがいらない場合はコメントアウト*/
    -- SELECT
    --     device_aid AS adid,
    --     DATETIME(TIMESTAMP_SECONDS(timestamp), 'Asia/Tokyo') AS jst_time,
    --     longitude,
    --     latitude --,app_id AS application_id
    -- FROM
    --     `beaconbank-analytics.from_Predicio.PD_locationlog_72H*`
    -- WHERE
        -- (
        --     (
        --         _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", DATE_SUB(start_date1, INTERVAL 1 DAY))
        --         AND FORMAT_DATE("%Y%m%d", end_date1)
        --         AND DATE(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') BETWEEN start_date1
        --         AND end_date1
        --     )
        --     OR (
        --         _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", DATE_SUB(start_date2, INTERVAL 1 DAY))
        --         AND FORMAT_DATE("%Y%m%d", end_date2)
        --         AND DATE(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') BETWEEN start_date2
        --         AND end_date2
        --     )
        -- )
    --     AND latitude BETWEEN lat1
    --     AND lat2
    --     AND longitude BETWEEN lon1
    --     AND lon2
    UNION ALL /*要変更点#3_2 Verasetのデータがいらない場合はコメントアウト*/
    SELECT
        ad_id AS id,
        DATETIME(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') AS jst_time,
        longitude,
        latitude --,CAST(hashed_provider_id_source_id AS STRING) AS application_id
    FROM
        `beaconbank-analytics.from_veraset.VS_locationlog*`
    WHERE
        (
            (
                _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", DATE_SUB(start_date1, INTERVAL 1 DAY))
                AND FORMAT_DATE("%Y%m%d", end_date1)
                AND DATE(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') BETWEEN start_date1
                AND end_date1
            )
            OR (
                _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", DATE_SUB(start_date2, INTERVAL 1 DAY))
                AND FORMAT_DATE("%Y%m%d", end_date2)
                AND DATE(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') BETWEEN start_date2
                AND end_date2
            )
        )
        AND latitude BETWEEN lat1
        AND lat2
        AND longitude BETWEEN lon1
        AND lon2
),
-- beacon_log AS(
--     /*対象店舗のビーコンログを抽出*/
--     SELECT
--         DISTINCT 
--         adid,
--         -- IF (adid IS NULL, CAST(app_user_id AS STRING),adid) AS id,
--         DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo') AS jst_time,
--         --detected_timeはUTCなのでJSTに変換
--         EXTRACT(DATE FROM DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo')) AS jst_date,
--         EXTRACT(HOUR FROM DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo')) AS HOUR,
--         DATE_TRUNC(CAST(DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo') AS DATE),MONTH) AS jst_ym,
--         target_store_name AS store_name,
--         longitude,
--         latitude
--     FROM
--         `beaconbank-analytics.daily_copy.BB_beaconlog*`
--     WHERE
--         _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date1)
--         AND FORMAT_DATE("%Y%m%d", end_date1)
--         AND event = 0
--         AND beacon_id IN (6755399122093398, 6755399122093397) /*要変更点#1*/
-- ),
tmp AS( /* 一時ファイルの作成 */ 
    SELECT
        -- DISTINCT 
        id,
        -- jst_time,
        EXTRACT(DATE FROM jst_time) AS jst_date,
        -- EXTRACT(HOUR FROM jst_time) AS HOUR,
        DATE_TRUNC(CAST(jst_time AS DATE), MONTH) AS jst_ym,
        CASE 
            WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly1), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name1
            -- WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly2), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name1
            -- WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly3), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name3
            -- WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly4), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name4
        END AS store_name,
        -- log.longitude,
        -- log.latitude,
        "gps" AS log_type,
        CASE
            WHEN
            EXTRACT(DATE FROM jst_time) <= separate_date
            THEN "before"
            ELSE "after"
        END AS flag
    FROM
        union_logs AS log
    WHERE
        (   ST_COVERS(ST_GEOGFROMTEXT(target_poly1), ST_GEOGPOINT(log.longitude, log.latitude))
        AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time1
        AND closing_time1
        )
        -- OR
        -- (   ST_COVERS(ST_GEOGFROMTEXT(target_poly2), ST_GEOGPOINT(log.longitude, log.latitude))
        -- AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time2
        -- AND closing_time2
        -- )
        -- OR
        -- (   ST_COVERS(ST_GEOGFROMTEXT(target_poly3), ST_GEOGPOINT(log.longitude, log.latitude))
        -- AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time3
        -- AND closing_time3
        -- )
        -- OR
        -- (   ST_COVERS(ST_GEOGFROMTEXT(target_poly4), ST_GEOGPOINT(log.longitude, log.latitude))
        -- AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time4
        -- AND closing_time4
        -- )
    -- UNION ALL
    -- SELECT
    --     *,
    --     "beacon" AS log_type
    -- FROM
    --     beacon_log
    -- ORDER BY
    --     log_type
),
date_cnt AS(
    SELECT
        id,
        COUNT(DISTINCT jst_date) AS cnt_date,
    FROM
        tmp
    GROUP BY 1
),
log_cnt AS(
    SELECT
        id,
        jst_date,
        COUNT(*) AS cnt_log,
    FROM
        tmp
    GROUP BY 1,2
),

profile AS (
    SELECT DISTINCT
        id,
        cnt_log,
        cnt_date,
        -- jst_time,
        -- jst_date,
        -- HOUR,
        -- jst_ym,
        store_name,
        -- longitude,
        -- latitude,
        log_type,
        flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
    FROM
        tmp
        LEFT JOIN log_cnt USING(id)
        LEFT JOIN date_cnt USING(id)
        LEFT JOIN `beaconbank-analytics.user_profile.AllUserProfile` a ON tmp.id = a.adid
    -- GROUP BY
    --     id,
    --     -- jst_time,
    --     -- jst_date,
    --     -- HOUR,
    --     -- jst_ym,
    --     store_name,
    --     longitude,
    --     latitude,
    --     log_type,
    --     flag,
    --     Gender,
    --     Age,
    --     home_lat,
    --     home_lon,
    --     home_pref,
    --     home_city,
    --     office_pref,
    --     office_city
    WHERE /* 来訪日数による足切り */
        cnt_date >= min_visit_cnt 
        AND cnt_date <= max_visit_cnt 
        AND cnt_log >= min_log_cnt
    ORDER BY
    cnt_date DESC
)
SELECT *
FROM profile
)
;