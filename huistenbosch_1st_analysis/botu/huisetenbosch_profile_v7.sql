/*分析 来訪頻度を指定してログを抽出_直接入力 */
-- 分析対象店舗のポリゴンを入力してログを取得
-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date,
extended_start_date DATE;

DECLARE 
 min_visit_cnt,
 max_visit_cnt,
 min_log_cnt INT64;

DECLARE lat1,
lat2,
lon1,
lon2 INT64;

DECLARE target_lat1,
target_lon1 FLOAT64;
DECLARE radius1,
opening_time1,
closing_time1 INT64;
DECLARE target_name1 STRING;
DECLARE target_poly1 STRING;

-- ★★★設定項目★★★
-- ターゲット期間1
SET start_date1 = '2019-09-01';
SET end_date1 = '2020-2-29';

-- ターゲット期間2
SET start_date2 = '2020-03-01';
SET end_date2 = '2021-05-31';

-- 前後比較の区切りの日付(flag=before,afterの区切り)
-- 日付 = separate_dateの場合はbeforeのフラグがつく
SET separate_date = '2020-02-29';

-- リピーター調査用の期間（2020/1/1の半年前）
SET extended_start_date = '2019-07-01';

-- ログ検索範囲設定用
SET lat1 = 23;
SET lat2 = 46;
SET lon1 = 123;
SET lon2 = 148;

-- 来訪判定をするログ数の下限（=2：来訪ログが2つ以上ない場合は削除）
SET min_log_cnt = 2;

-- 来訪日数の下限（=3：来訪日数が3日以上のログを抽出）
SET min_visit_cnt = 1;
-- 来訪日数の上限（=20：来訪日数が20日以下のログを抽出）
-- SET max_visit_cnt = 9999;

-- 対象店舗/エリア1のポリゴン・抽出半径・営業時間情報
-- SET target_lat1 = 36.56183463;
-- SET target_lon1 = 139.8629231;
-- SET radius1 = 80;
SET opening_time1 = 9;
SET closing_time1 = 22;
SET target_name1 = 'ハウステンボス';
SET target_poly1 = 'POLYGON((129.78205143090764 33.07835732316272,129.78219090577642 33.07812358391031,129.78372512933294 33.0782629285394,129.78587626096288 33.07994094642577,129.78602110024968 33.08198610156933,129.78632687207738 33.08196812239075,129.78637515183965 33.08225578880849,129.7859398408834 33.082355053138926,129.78600113514284 33.08309912492111,129.78626627728738 33.083227414448146,129.78640953038254 33.08304388872046,129.78917976212063 33.08287011053718,129.78980105426348 33.08414065430174,129.79152107175383 33.08433100348544,129.79110945458916 33.08502632589806,129.7910586964561 33.08620121471334,129.7913380538701 33.087036299631215,129.79091508019866 33.08714236290895,129.79166772942864 33.08871182359587,129.79188661906466 33.08888124879444,129.7924702891267 33.08883494061111,129.7925948291466 33.08998005584629,129.78986665717986 33.090216336850176,129.7891685520388 33.090225977745845,129.7890900371802 33.090963703982716,129.78769187548625 33.09082952596329,129.78692805622433 33.09023917012475,129.78641100019178 33.08956791103889,129.78771577438766 33.08878269066757,129.78662631556818 33.087610355058665,129.78576168034652 33.08756685566208,129.7858348543612 33.08664836788825,129.7855486123679 33.08584448281953,129.78529455688277 33.08520239722506,129.7836560624508 33.08432402190212,129.7840966152815 33.08369092888635,129.7829821574358 33.08192131330081,129.78253959294835 33.08009005051815,129.7823303806452 33.07992900685026,129.78205143090764 33.07835732316272))'
;

-- その他変更点(#numberでコード内検索)
-- #0 出力する一次テーブル名
-- #1 beacon_idの指定（beaconログを使用する場合）
-- #2 分析対象店舗テーブル名
-- #3 外部データの有無
-- 　対象店舗を増やす場合はtarget_lat3などで検索して、対象箇所をコメントインする

-- ★★★設定終了★★★

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210607_huistenbosch_profile_repeater` AS /*要変更点#0*/(
    
WITH union_logs AS(
    /*BeaconBankのロケーションログを抽出*/
    SELECT
        -- adid,
        IF (adid IS NULL,CAST(app_user_id AS STRING),UPPER(adid)) AS id,
        DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo') AS jst_time,
        --detected_timeはUTCなのでJSTに変換
        longitude,
        latitude
    FROM
        `beaconbank-analytics.daily_copy.BB_locationlog*`
    WHERE
        (
            _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", extended_start_date)
            AND FORMAT_DATE("%Y%m%d", end_date1)
            OR _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date2)
            AND FORMAT_DATE("%Y%m%d", end_date2)
        )
        AND latitude BETWEEN lat1 AND lat2
        AND longitude BETWEEN lon1 AND lon2

    UNION ALL /*要変更点#3_2 Verasetのデータがいらない場合はコメントアウト*/

    SELECT
        UPPER(ad_id) AS id,
        DATETIME(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') AS jst_time,
        longitude,
        latitude --,CAST(hashed_provider_id_source_id AS STRING) AS application_id
    FROM
        `beaconbank-analytics.from_veraset.VS_locationlog*`
    WHERE
        (
            (
                _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", DATE_SUB(extended_start_date, INTERVAL 1 DAY))
                AND FORMAT_DATE("%Y%m%d", end_date1)
                AND DATE(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') BETWEEN extended_start_date
                AND end_date1
            )
            OR (
                _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", DATE_SUB(start_date2, INTERVAL 1 DAY))
                AND FORMAT_DATE("%Y%m%d", end_date2)
                AND DATE(TIMESTAMP_SECONDS(utc_timestamp), 'Asia/Tokyo') BETWEEN start_date2
                AND end_date2
            )
        )
        AND latitude BETWEEN lat1 AND lat2
        AND longitude BETWEEN lon1 AND lon2
),
-- beacon_log AS(
--     /*対象店舗のビーコンログを抽出*/
--     SELECT
--         DISTINCT 
--         adid,
--         -- IF (adid IS NULL, CAST(app_user_id AS STRING),adid) AS id,
--         DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo') AS jst_time,
--         --detected_timeはUTCなのでJSTに変換
--         EXTRACT(DATE FROM DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo')) AS jst_date,
--         EXTRACT(HOUR FROM DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo')) AS HOUR,
--         DATE_TRUNC(CAST(DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo') AS DATE),MONTH) AS jst_ym,
--         target_store_name AS store_name,
--         longitude,
--         latitude
--     FROM
--         `beaconbank-analytics.daily_copy.BB_beaconlog*`
--     WHERE
--         _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date1)
--         AND FORMAT_DATE("%Y%m%d", end_date1)
--         AND event = 0
--         AND beacon_id IN (6755399122093398, 6755399122093397) /*要変更点#1*/
-- ),
tmp AS( /* 1次ファイルの作成 */ 
    SELECT
        -- DISTINCT 
        id,
        -- jst_time,
        EXTRACT(DATE FROM jst_time) AS jst_date,
        -- EXTRACT(HOUR FROM jst_time) AS HOUR,
        DATE_TRUNC(CAST(jst_time AS DATE), MONTH) AS jst_ym,
        CASE 
            WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly1), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name1
            -- WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly2), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name1
            -- WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly3), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name3
            -- WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly4), ST_GEOGPOINT(log.longitude, log.latitude)) THEN target_name4
        END AS store_name,
        -- log.longitude,
        -- log.latitude,
        "gps" AS log_type,
        CASE
            WHEN
            EXTRACT(DATE FROM jst_time) <= separate_date
            THEN "before"
            ELSE "after"
        END AS flag
    FROM
        union_logs AS log
    WHERE
        (   ST_COVERS(ST_GEOGFROMTEXT(target_poly1), ST_GEOGPOINT(log.longitude, log.latitude))
        AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time1
        AND closing_time1
        )
        -- OR
        -- (   ST_COVERS(ST_GEOGFROMTEXT(target_poly2), ST_GEOGPOINT(log.longitude, log.latitude))
        -- AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time2
        -- AND closing_time2
        -- )
        -- OR
        -- (   ST_COVERS(ST_GEOGFROMTEXT(target_poly3), ST_GEOGPOINT(log.longitude, log.latitude))
        -- AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time3
        -- AND closing_time3
        -- )
        -- OR
        -- (   ST_COVERS(ST_GEOGFROMTEXT(target_poly4), ST_GEOGPOINT(log.longitude, log.latitude))
        -- AND EXTRACT(HOUR FROM jst_time) BETWEEN opening_time4
        -- AND closing_time4
        -- )
    -- UNION ALL
    -- SELECT
    --     *,
    --     "beacon" AS log_type
    -- FROM
    --     beacon_log
    -- ORDER BY
    --     log_type
),
date_cnt AS(
    SELECT
        id,
        flag,
        COUNT(DISTINCT jst_date) AS cnt_date,
        CASE
            WHEN flag = 'before' THEN COUNT(DISTINCT jst_date) / 6
            WHEN flag = 'after' THEN COUNT(DISTINCT jst_date) / 12.5
        END AS visit_per_month,
    FROM
        tmp
    WHERE 
        jst_date BETWEEN start_date1 AND end_date2
    GROUP BY 1,2
),
log_cnt AS(
    SELECT
        id,
        jst_date,
        jst_date - 180 AS before_half_year,
        COUNT(*) AS cnt_log,
    FROM
        tmp
    GROUP BY 1,2,3
),
repeat AS(
    SELECT
        id,
        jst_date,
        before_half_year,
        LAG(jst_date) OVER(PARTITION BY id ORDER BY jst_date) AS last_visited,
        CASE
            WHEN before_half_year <= LAG(jst_date)
            OVER(PARTITION BY id ORDER BY jst_date)
                THEN 'repeat'
            WHEN extended_start_date <= LAG(jst_date)
            OVER(PARTITION BY id ORDER BY jst_date)
                THEN 'come_back'
            ELSE 'new'
        END AS repeat_flag,
    FROM
        (SELECT
            *
        FROM
            log_cnt
        WHERE　/* 来訪ログ数による足切り */
            cnt_log >= min_visit_cnt 
        )
),


profile AS (
    SELECT DISTINCT
        id,
        -- cnt_log,
        cnt_date,
        visit_per_month,
        -- jst_time,
        jst_date,
        -- HOUR,
        -- jst_ym,
        store_name,
        -- longitude,
        -- latitude,
        log_type,
        t0.flag,
        repeat_flag,
        CASE
            WHEN t4.prefecture IN ('長崎県','福岡県','佐賀県') THEN '近隣' 
            WHEN t4.prefecture IN ('熊本県','大分県','鹿児島県','宮崎県') THEN 'その他九州'
            ELSE '遠方'
        END AS home_flag,
        t2.Gender,
        t3.Age,
        t4.latitude AS home_lat,
        t4.longitude AS home_lon,
        t4.prefecture AS home_pref,
        t4.city AS home_city,
        t5.prefecture AS office_pref,
        t5.city AS office_city,
    FROM
        (SELECT 
            * 
        FROM
            tmp
        WHERE
            jst_date BETWEEN start_date1 AND end_date2
        ) t0
        -- LEFT JOIN log_cnt USING(id)
        LEFT JOIN date_cnt USING(id,flag)
        LEFT JOIN repeat USING(id,jst_date)
        LEFT JOIN (SELECT UPPER(id) AS id, Gender   FROM `beaconbank-analytics.user_profile_v2.vw_gender`) t2 USING(id)
        LEFT JOIN (SELECT UPPER(id) AS id, Age      FROM `beaconbank-analytics.user_profile_v2.vw_age_fac`) t3 USING(id)
        LEFT JOIN (SELECT UPPER(id) AS id, latitude,longitude,prefecture,city FROM `beaconbank-analytics.user_profile_v2.vw_home_4m`) t4 USING(id)
        LEFT JOIN (SELECT UPPER(id) AS id, prefecture,city FROM `beaconbank-analytics.user_profile_v2.vw_office_4m`) t5 USING(id)
    WHERE /* 来訪日数 */
        cnt_date >= min_visit_cnt 
        -- AND cnt_date <= max_visit_cnt 
        -- AND cnt_log >= min_log_cnt
    ORDER BY
    cnt_date DESC
)
SELECT *
FROM profile
)
;