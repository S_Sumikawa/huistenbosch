-- DECLARE x STRING DEFAULT '20200930'; 
-- LOOP
-- INSERT INTO user_profile.UP_DNA_MaaS_167_daily

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210607_huistenbosch_03_division_range` 
PARTITION BY date
CLUSTER BY id
AS

WITH
  division_range AS (
    SELECT
      date,
      id,
--       lag_adid,
      jst_time,
--       lag_time,
      latitude,
--       lag_lat,
      longitude,
--       lag_lon,
      event_visit,
--       lag_event_visit,
      DATETIME_DIFF(jst_time, lag_time, SECOND) AS dif_second,
      ST_DISTANCE(ST_GEOGPOINT(longitude, latitude), ST_GEOGPOINT(lag_lon, lag_lat)) AS dif_dist,
      CASE
        WHEN (CASE WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1 ELSE 0 END) = 1 THEN 0
        ELSE DATETIME_DIFF(jst_time, lag_time, SECOND) 
      END AS dif_second_used,
      CASE
        WHEN (CASE WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1 ELSE 0 END) = 1 THEN 0
        ELSE ST_DISTANCE(ST_GEOGPOINT(longitude, latitude), ST_GEOGPOINT(lag_lon, lag_lat)) 
      END AS dif_dist_used,
---- 分割定義 ①ユーザが異なる/後続のデータが無い　②トリップでエリアに入っている/エリアから出ている　③同じユーザでも前後のログ間隔が10分以上ある
---- この場合はcut = 1 でデータは使用されない dif_second_used, dif_dist_used は使用可能な場合のみ トリップ所要時間/ トリップ距離が入る
      CASE
        WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1
        ELSE 0 END AS cut,
      SUM(CASE
        WHEN lag_adid IS NULL OR event_visit != lag_event_visit OR DATETIME_DIFF(jst_time, lag_time, SECOND) >= 600 THEN 1
        ELSE 0 
      END) OVER(ORDER BY id, jst_time) AS division_id
--     FROM log_sameday
    FROM `beaconbank-analytics.huistenbosch.20210607_huistenbosch_02_log_sameday`
    GROUP BY
      date,
      id,
      lag_adid,
      lag_event_visit,
      lag_time,
      lag_lat,
      lag_lon,
      jst_time,
      latitude,
      longitude,
      event_visit,
      dif_second,
      dif_dist
  ),

  --滞在時間算出
  stay_time AS (
    SELECT
      division_id AS division_id_stay,
      SUM(dif_second_used ) AS stay_second,
      SUM(dif_second_used ) / 60 AS stay_minit,
      (SUM(dif_second_used ) / 60 ) /60 AS stay_hour,
      CAST( (SUM(dif_second_used ) / 60 ) /60 AS int64) AS stay_hour_int
    FROM division_range
    WHERE dif_second IS NOT NULL
    GROUP BY division_id 
            ),
            TL AS (
                SELECT *
                FROM `beaconbank-analytics.place_master.TrainLines`
                WHERE Company IN ('九州旅客鉄道') AND LineName IN ('大村線')
            ),

            log_visitor AS (
                SELECT
                c.date,
                c.id,
            --       lag_adid,
                jst_time,
            --       lag_time,
            --       dif_second,
                c.latitude,
                c.longitude,
                division_id,
            --       lag_lat,
            --       lag_lon,
            --       dif_dist,
                event_visit,
            --       lag_event_visit,
                cut,
                dif_second_used,
                dif_dist_used,
            --       division_id,
                (dif_dist/1000) / (dif_second/3600) AS velocity,
                CASE
                    -- WHEN ST_DWITHIN(ST_GEOGFROMGEOJSON(TL_road.geom), ST_GEOGPOINT(c.longitude, c.latitude), 2) THEN 'Train'
                    WHEN ST_DWITHIN(ST_GEOGFROMGEOJSON(TL.geom), ST_GEOGPOINT(c.longitude, c.latitude), 20) THEN 'Train'
                    WHEN (dif_dist/1000) / (dif_second/3600) >= 5 THEN 'Car'
                    ELSE 'Walk'
                END AS transportation,
                stay_second,
                stay_minit,
                stay_hour,
                stay_hour_int,
                FROM (
                SELECT *
                FROM division_range a
                INNER JOIN stay_time b ON a.division_id = b.division_id_stay
                ORDER BY
                    id,
                    jst_time
                ) c
                -- , TL_road
                , TL
                WHERE dif_second > 1
  )

SELECT *
FROM division_range
