CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210706_huistenbosch_strolling_beacon2_v2` AS
-- beacon2はbeacon + GPS(5m)で回遊率を算出

WITH t0 AS(
  SELECT
    DISTINCT
    id,
    jst_date,
    area,
    place_name
  FROM
    `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3`
    WHERE
         (buffer IS NULL OR buffer = 5)
        AND place_name IS NOT NULL
        AND hour BETWEEN 9 AND 20
        AND id IS NOT NULL
        AND (visit_per_month <= 15 OR visit_per_month IS NULL)
)
, t1 AS(
  SELECT
    DISTINCT
    id,
    jst_date,
    area,
    place_name
  FROM
    `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3`
    WHERE
         (buffer IS NULL OR buffer = 5)
        AND place_name IS NOT NULL
        AND hour BETWEEN 9 AND 20
        AND id IS NOT NULL
        AND (visit_per_month <= 15 OR visit_per_month IS NULL)
)
, t2 AS(
  SELECT DISTINCT 
    t0.id,
    t0.jst_date,
    t0.place_name AS base_place_name1,
    t1.place_name AS move_place_name1
  FROM
    t0
    LEFT JOIN t1
    USING(id, jst_date)
  ORDER BY
    2,
    3,
    4
)
,t3 AS(
  SELECT
    t2.base_place_name1,
    t2.move_place_name1,
    COUNT(*) AS cnt,
    CASE
      WHEN base_place_name1 = move_place_name1 THEN 1
      ELSE 0
    END AS base_flag
  FROM
    t2
  GROUP BY
    1,
    2
  ORDER BY
    1,
    2
)
, t4 AS (
  SELECT
    t3.base_place_name1,
    cnt AS cnt_max
  FROM
    t3
  WHERE
    base_flag =1
)
, t5 AS(
  SELECT
    t3.base_place_name1,
    t3.move_place_name1,
    t3.cnt,
    cnt_max
  FROM
    t3
    LEFT JOIN t4
    USING(base_place_name1)
)
  --     ,
  --   t6 AS(
  SELECT
    *,
    ROUND(SAFE_DIVIDE(cnt,cnt_max),4) AS ratio
  FROM
    t5
  WHERE
    base_place_name1 IS NOT NULL
    AND move_place_name1 IS NOT NULL
  ORDER BY
    1,
    2
    --     ),
  --   t7 AS(
    --   SELECT
    --     t6.*,
    --     master.floor AS base_floor
    --   FROM
    --     t6
    --   LEFT JOIN
    --     master
    --   ON
    --     base_place_name1 = place_name)
  -- SELECT DISTINCT
  --   t7.*,
  --   master.floor AS move_floor
  -- FROM
  --   t7
  -- LEFT JOIN
  --   master
  -- ON
  --   move_place_name1 = place_name
  -- WHERE base_floor IS NOT NULL AND master.floor IS NOT N