CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210701_huistenbosch_strolling_area` AS
-- areaはGPS ＋ beaconで回遊率を算出

WITH t0 AS(
  SELECT
    DISTINCT
    id,
    jst_date,
    area,
    -- place_name
  FROM
    `beaconbank-analytics.huistenbosch.20210701_huistenbosch_beacon_visitor`
    WHERE
        data_from = 'beacon'
        AND area IS NOT NULL
  UNION DISTINCT
  SELECT
    DISTINCT
    id,
    jst_date,
    area,
    -- place_name
  FROM
    `beaconbank-analytics.huistenbosch.20210629_huistenbosch_behavior_inside_park`
  WHERE
    area IS NOT NULL
)
, t1 AS(
  SELECT
    DISTINCT
    id,
    jst_date,
    area,
    -- place_name
  FROM
    `beaconbank-analytics.huistenbosch.20210701_huistenbosch_beacon_visitor`
    WHERE
        data_from = 'beacon'
        AND area IS NOT NULL
  UNION DISTINCT
  SELECT
    DISTINCT
    id,
    jst_date,
    area,
    -- place_name
  FROM
    `beaconbank-analytics.huistenbosch.20210629_huistenbosch_behavior_inside_park`
  WHERE
    area IS NOT NULL
)
, t2 AS(
  SELECT DISTINCT 
    t0.id,
    t0.jst_date,
    t0.area AS base_area1,
    t1.area AS move_area1
  FROM
    t0
    LEFT JOIN t1
    USING(id, jst_date)
  ORDER BY
    2,
    3,
    4
)
,t3 AS(
  SELECT
    t2.base_area1,
    t2.move_area1,
    COUNT(*) AS cnt,
    CASE
      WHEN base_area1 = move_area1 THEN 1
      ELSE 0
    END AS base_flag
  FROM
    t2
  GROUP BY
    1,
    2
  ORDER BY
    1,
    2
)
, t4 AS (
  SELECT
    t3.base_area1,
    cnt AS cnt_max
  FROM
    t3
  WHERE
    base_flag =1
)
, t5 AS(
  SELECT
    t3.base_area1,
    t3.move_area1,
    t3.cnt,
    cnt_max
  FROM
    t3
    LEFT JOIN t4
    USING(base_area1)
)
  --     ,
  --   t6 AS(
  SELECT
    *,
    ROUND(SAFE_DIVIDE(cnt,cnt_max),4) AS ratio
  FROM
    t5
  WHERE
    base_area1 IS NOT NULL
    AND move_area1 IS NOT NULL
  ORDER BY
    1,
    2
    --     ),
  --   t7 AS(
    --   SELECT
    --     t6.*,
    --     master.floor AS base_floor
    --   FROM
    --     t6
    --   LEFT JOIN
    --     master
    --   ON
    --     base_area1 = area)
  -- SELECT DISTINCT
  --   t7.*,
  --   master.floor AS move_floor
  -- FROM
  --   t7
  -- LEFT JOIN
  --   master
  -- ON
  --   move_area1 = area
  -- WHERE base_floor IS NOT NULL AND master.floor IS NOT N