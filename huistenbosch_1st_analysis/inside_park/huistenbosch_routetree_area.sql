CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210705_huistenbosch_routetree_area` AS

WITH t0 AS( -- areaの変わり目にフラグを立てる
    SELECT
        id,
        jst_time,
        jst_date,
        flag,
        visit_flag,
        repeat_flag,
        cnt_date,
        visit_per_month,
        home_flag,
        night_before,
        night_after,
        recent_visit,
        next_visit,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        data_from,
        beacon_id,
        uuid,
        major,
        minor,
        area,
        buffer,
        place_name,
        ROW_NUMBER() OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS log_number,
        CASE
            WHEN LAG(area) OVER(PARTITION BY id, jst_date ORDER BY jst_time) = area THEN NULL
            WHEN LAG(area) OVER(PARTITION BY id, jst_date ORDER BY jst_time) IS NULL THEN NULL
            -- WHEN LAG(area) OVER(PARTITION BY id, jst_date ORDER BY jst_time) IN ('', '') THEN NULL
            ELSE 1
        END AS area_change_flag,
    FROM 
        `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3`
    WHERE
        (visit_per_month IS NULL OR visit_per_month <= 15)
        AND (buffer IS NULL OR buffer = 5)
        AND id IS NOT NULL
        AND hour BETWEEN 9 AND 20
        AND area IS NOT NULL
)
, t1 AS( -- areaごとのログ数を算出
    SELECT
        *,
        LEAD(log_number) OVER(PARTITION BY id, jst_date ORDER BY jst_time) - log_number AS log_cnt,
        MAX(log_number) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS max_log_number,
    FROM 
        t0
    WHERE
        area_change_flag = 1
)
, t2 AS( --エリア来訪のログのみを並べる
    SELECT
        *,
        ROW_NUMBER() OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS visit_area_number,
    FROM
        t1
    WHERE
        log_cnt >=2  --ログ数2以上を滞在とする
        OR (log_cnt IS NULL AND max_log_number - log_cnt >= 2) -- 最後に滞在したエリアだけ計算方法が変わる
)
SELECT
    id,
    jst_date,
    flag,
    visit_flag,
    repeat_flag,
    cnt_date,
    visit_per_month,
    home_flag,
    night_before,
    night_after,
    recent_visit,
    next_visit,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    data_from,
    beacon_id,
    uuid,
    major,
    minor,
    buffer,
    visit_area_number,
    -- place_name,
    LAG(area,5) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_area5,
    LAG(area,4) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_area4,
    LAG(area,3) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_area3,
    LAG(area,2) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_area2,
    LAG(area) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_area1,
    area,
    LEAD(area) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area1,
    LEAD(area,2) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area2,
    LEAD(area,3) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area3,
    LEAD(area,4) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area4,
    LEAD(area,5) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area5,
    LEAD(area,6) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area6,
    LEAD(area,7) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area7,
    LEAD(area,8) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area8,
    LEAD(area,9) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area9,
    LEAD(area,10) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_area10,
FROM
    t2
ORDER BY
    id, jst_date, visit_area_number