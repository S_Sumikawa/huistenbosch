CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210701_huistenbosch_beacon_visitor_v2` AS

SELECT DISTINCT
    *,
    NULL AS buffer
FROM
    `beaconbank-analytics.huistenbosch.20210701_huistenbosch_beaconlog_profile_v2`

UNION ALL

SELECT DISTINCT
    id,
    TIMESTAMP(jst_time),
    jst_date,
    hour,
    flag,
    visit_flag,
    repeat_flag,
    cnt_date,
    visit_per_month,
    home_flag,
    night_after,
    night_after,
    recent_visit,
    next_visit,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    'gps' AS data_from,
    NULL AS beacon_id,
    uuid,
    major,
    minor,
    t1.area,
    place_name,
    5 AS buffer
FROM
    `beaconbank-analytics.huistenbosch.20210629_huistenbosch_behavior_inside_park` t0
    , `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon` t1
WHERE
    ST_DWITHIN(ST_GEOGPOINT(t0.longitude,t0.latitude), ST_GEOGPOINT(t1.longetude, t1.latitude),5)

UNION ALL

SELECT DISTINCT
    id,
    TIMESTAMP(jst_time),
    jst_date,
    hour,
    flag,
    visit_flag,
    repeat_flag,
    cnt_date,
    visit_per_month,
    home_flag,
    night_after,
    night_after,
    recent_visit,
    next_visit,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    'gps' AS data_from,
    NULL AS beacon_id,
    uuid,
    major,
    minor,
    t1.area,
    place_name,
    3 AS buffer
FROM
    `beaconbank-analytics.huistenbosch.20210629_huistenbosch_behavior_inside_park` t0
    , `beaconbank-analytics.huistenbosch.huistenbosch_beacon_latlon` t1
WHERE
    ST_DWITHIN(ST_GEOGPOINT(t0.longitude,t0.latitude), ST_GEOGPOINT(t1.longetude, t1.latitude),3)
