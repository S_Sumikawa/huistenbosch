CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210705_huistenbosch_routetree_beacon` AS

WITH prepare AS(
    SELECT
        id,
        jst_time,
        jst_date,
        hour,
        flag,
        visit_flag,
        repeat_flag,
        cnt_date,
        visit_per_month,
        home_flag,
        night_before,
        night_after,
        recent_visit,
        next_visit,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        data_from,
        beacon_id,
        uuid,
        major,
        minor,
        area,
        buffer,
        CASE
            WHEN place_name = 'カナルクルーザー' AND area = 'タワーシティ' THEN 'カナルクルーザー(タワーシティ)'
            WHEN place_name = 'カナルクルーザー' AND area = 'ウェルカムエリア' THEN 'カナルクルーザー(ウェルカムエリア)'
            ELSE place_name
        END AS place_name,
    FROM 
        `beaconbank-analytics.huistenbosch.20210705_huistenbosch_beacon_visitor_v3`
)
, t0 AS( -- areaの変わり目にフラグを立てる
    SELECT
        id,
        jst_time,
        jst_date,
        flag,
        visit_flag,
        repeat_flag,
        cnt_date,
        visit_per_month,
        home_flag,
        night_before,
        night_after,
        recent_visit,
        next_visit,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        data_from,
        beacon_id,
        uuid,
        major,
        minor,
        area,
        buffer,
        place_name,
        ROW_NUMBER() OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS log_number,
        CASE
            WHEN LAG(place_name) OVER(PARTITION BY id, jst_date ORDER BY jst_time) = place_name THEN NULL
            ELSE 1
        END AS area_change_flag,
    FROM 
        prepare
    WHERE
        (visit_per_month IS NULL OR visit_per_month <= 15)
        AND (buffer IS NULL OR buffer = 5)
        AND id IS NOT NULL
        AND hour BETWEEN 9 AND 20
        AND place_name IS NOT NULL
)
, t1 AS( -- areaごとのログ数を算出
    SELECT
        *,
        LEAD(log_number) OVER(PARTITION BY id, jst_date ORDER BY jst_time) - log_number AS log_cnt,
        MAX(log_number) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS max_log_number,
    FROM 
        t0
    WHERE
        area_change_flag = 1
)
, t2 AS( --エリア来訪のログのみを並べる
    SELECT
        *,
        ROW_NUMBER() OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS visit_area_number,
    FROM
        t1
    -- WHERE
    --     log_cnt >=2  --ログ数2以上を滞在とする
    --     OR (log_cnt IS NULL AND max_log_number - log_cnt >= 2) -- 最後に滞在したエリアだけ計算方法が変わる
)
SELECT
    id,
    jst_date,
    flag,
    visit_flag,
    repeat_flag,
    cnt_date,
    visit_per_month,
    home_flag,
    night_before,
    night_after,
    recent_visit,
    next_visit,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    data_from,
    beacon_id,
    uuid,
    major,
    minor,
    area,
    buffer,
    visit_area_number,
    LAG(place_name,5) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_place5,
    LAG(place_name,4) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_place4,
    LAG(place_name,3) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_place3,
    LAG(place_name,2) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_place2,
    LAG(place_name) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS prev_place1,
    place_name,
    LEAD(place_name) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place1,
    LEAD(place_name,2) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place2,
    LEAD(place_name,3) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place3,
    LEAD(place_name,4) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place4,
    LEAD(place_name,5) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place5,
    LEAD(place_name,6) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place6,
    LEAD(place_name,7) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place7,
    LEAD(place_name,8) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place8,
    LEAD(place_name,9) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place9,
    LEAD(place_name,10) OVER(PARTITION BY id, jst_date ORDER BY jst_time) AS next_place10,
FROM
    t2
ORDER BY
    id, jst_date, visit_area_number