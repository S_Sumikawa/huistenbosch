CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_ML_ratio` AS /*要変更点#0*/

WITH sum_logcnt AS(
SELECT  
	device_id,
  datetime,
  SUM(cnt) AS sum_cnt
FROM `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_log_for_ML_v2`
GROUP BY 1,2
),
ratio AS(
SELECT  
  device_id,
  datetime,
  hour,
  cnt / sum_cnt AS ratio
FROM `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_log_for_ML_v2`
JOIN sum_logcnt USING(device_id, datetime)
)

SELECT DISTINCT *
FROM ratio
ORDER BY 1,2,3