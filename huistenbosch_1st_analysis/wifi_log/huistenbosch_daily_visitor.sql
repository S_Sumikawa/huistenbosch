WITH t0 AS
(SELECT  
device_id, datetime,
sum( cnt) AS DailyVisitor
FROM `labs-analytics-298709.works_sumikawa.20210528_huistenbosch_wifi_log`
GROUP BY 1,2)

, t1 AS(SELECT  
device_id, datetime,
sum( cnt) AS DV_opening_hour
FROM `labs-analytics-298709.works_sumikawa.20210528_huistenbosch_wifi_log`
WHERE hour BETWEEN 9 AND 20
GROUP BY 1,2)

SELECT
*
FROM t0 JOIN t1 USING(device_id, datetime)