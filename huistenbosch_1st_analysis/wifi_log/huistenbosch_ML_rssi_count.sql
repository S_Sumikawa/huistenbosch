CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210624_huistenbosch_wifi_ML_rssi_count` AS /*要変更点#0*/

WITH
  LOG_BASE AS (
  SELECT
    *,
    TIMESTAMP(DATETIME(first_detected, 'Asia/Tokyo')) AS first_detected_jst,
    TIMESTAMP(DATETIME(last_detected, 'Asia/Tokyo')) AS last_detected_jst
  FROM
    `beaconbank-signage.wifi_log.wifilog_v3`
  WHERE
    DATE(last_detected,'Asia/Tokyo') BETWEEN "2021-06-01"
    AND "2021-06-30"
    AND device_id IN ('RP_1611307908901850490') ),
    
  LOG_DATA AS (
  SELECT
    log.mac_address,
    log.rssi,
    log.device_id,
    log.last_detected_jst,
    CAST(SUBSTR(FORMAT_TIMESTAMP('%Y%m%d%H%M', TIMESTAMP(CAST(log.last_detected_jst AS String))), 0, 8) AS INT64) AS DATETIME,
    CAST(SUBSTR(FORMAT_TIMESTAMP('%Y%m%d%H%M', TIMESTAMP(CAST(log.last_detected_jst AS String))), 9, 2) AS INT64) AS HOUR,
    EXTRACT(DAYOFWEEK FROM log.last_detected_jst) AS DAY_OF_WEEK,
    oui.Corporation
  FROM
    LOG_BASE log
  LEFT JOIN
    wifi_log.oui oui
  ON
    log.oui = oui.Oui
  WHERE
    (oui.Corporation IS NULL
      OR oui.Corporation LIKE "Apple%"
      OR oui.Corporation LIKE "Sony%"
      OR oui.Corporation LIKE "ASUS%"
      OR oui.Corporation LIKE "FUJITSU%"
      OR oui.Corporation LIKE "%OPPO%"
      OR oui.Corporation LIKE "HTC%"
      OR oui.Corporation LIKE "HUAWEI%"
      OR oui.Corporation LIKE "KYOCERA%"
      OR oui.Corporation LIKE "LG%"
      OR oui.Corporation LIKE "Motorola%"
      OR oui.Corporation LIKE "Samsung%"
      OR oui.Corporation LIKE "SHARP%"
      OR oui.Corporation LIKE "zte%"
      OR oui.Corporation LIKE "Xiaomi%"
      OR oui.Corporation LIKE "Covia%"
      OR oui.Corporation LIKE "Panasonic%"
      OR oui.Corporation LIKE "Lenovo Mobile%"
      OR oui.Corporation LIKE "OnePlus%"
      OR oui.Corporation LIKE "Essential%"
      OR oui.Corporation LIKE "TCT%"
      OR oui.Corporation LIKE "vivo%"
      OR oui.Corporation LIKE "Google%")
    AND log.Rssi < -1)
    
SELECT
  DATETIME,
  CASE
    WHEN base.DAY_OF_WEEK = 1 THEN "日曜日"
    WHEN base.DAY_OF_WEEK = 2 THEN "月曜日"
    WHEN base.DAY_OF_WEEK = 3 THEN "火曜日"
    WHEN base.DAY_OF_WEEK = 4 THEN "水曜日"
    WHEN base.DAY_OF_WEEK = 5 THEN "木曜日"
    WHEN base.DAY_OF_WEEK = 6 THEN "金曜日"
    ELSE "土曜日"
  END AS str_day_of_week,
--   CAST(base.HOUR AS string) AS hour,
--   base.HOUR AS HOUR_INT,
  base.DEVICE_ID,
  COUNTIF(rssi=-99) 	AS RSSI_99,
COUNTIF(rssi=-98) 	AS RSSI_98,
COUNTIF(rssi=-97)	AS RSSI_97,
COUNTIF(rssi=-96)	AS RSSI_96,
COUNTIF(rssi=-95)	AS RSSI_95,
COUNTIF(rssi=-94)	AS RSSI_94,
COUNTIF(rssi=-93)	AS RSSI_93,
COUNTIF(rssi=-92)	AS RSSI_92,
COUNTIF(rssi=-91)	AS RSSI_91,
COUNTIF(rssi=-90)	AS RSSI_90,
COUNTIF(rssi=-89)	AS RSSI_89,
COUNTIF(rssi=-88)	AS RSSI_88,
COUNTIF(rssi=-87)	AS RSSI_87,
COUNTIF(rssi=-86)	AS RSSI_86,
COUNTIF(rssi=-85)	AS RSSI_85,
COUNTIF(rssi=-84)	AS RSSI_84,
COUNTIF(rssi=-83)	AS RSSI_83,
COUNTIF(rssi=-82)	AS RSSI_82,
COUNTIF(rssi=-81)	AS RSSI_81,
COUNTIF(rssi=-80)	AS RSSI_80,
COUNTIF(rssi=-79)	AS RSSI_79,
COUNTIF(rssi=-78)	AS RSSI_78,
COUNTIF(rssi=-77)	AS RSSI_77,
COUNTIF(rssi=-76)	AS RSSI_76,
COUNTIF(rssi=-75)	AS RSSI_75,
COUNTIF(rssi=-74)	AS RSSI_74,
COUNTIF(rssi=-73)	AS RSSI_73,
COUNTIF(rssi=-72)	AS RSSI_72,
COUNTIF(rssi=-71)	AS RSSI_71,
COUNTIF(rssi=-70)	AS RSSI_70,
COUNTIF(rssi=-69)	AS RSSI_69,
COUNTIF(rssi=-68)	AS RSSI_68,
COUNTIF(rssi=-67)	AS RSSI_67,
COUNTIF(rssi=-66)	AS RSSI_66,
COUNTIF(rssi=-65)	AS RSSI_65,
COUNTIF(rssi=-64)	AS RSSI_64,
COUNTIF(rssi=-63)	AS RSSI_63,
COUNTIF(rssi=-62)	AS RSSI_62,
COUNTIF(rssi=-61)	AS RSSI_61,
COUNTIF(rssi=-60)	AS RSSI_60,
COUNTIF(rssi=-59)	AS RSSI_59,
COUNTIF(rssi=-58)	AS RSSI_58,
COUNTIF(rssi=-57)	AS RSSI_57,
COUNTIF(rssi=-56)	AS RSSI_56,
COUNTIF(rssi=-55)	AS RSSI_55,
COUNTIF(rssi=-54)	AS RSSI_54,
COUNTIF(rssi=-53)	AS RSSI_53,
COUNTIF(rssi=-52)	AS RSSI_52,
COUNTIF(rssi=-51)	AS RSSI_51,
COUNTIF(rssi=-50)	AS RSSI_50,
COUNTIF(rssi=-49)	AS RSSI_49,
COUNTIF(rssi=-48)	AS RSSI_48,
COUNTIF(rssi=-47)	AS RSSI_47,
COUNTIF(rssi=-46)	AS RSSI_46,
COUNTIF(rssi=-45)	AS RSSI_45,
COUNTIF(rssi=-44)	AS RSSI_44,
COUNTIF(rssi=-43)	AS RSSI_43,
COUNTIF(rssi=-42)	AS RSSI_42,
COUNTIF(rssi=-41)	AS RSSI_41,
COUNTIF(rssi=-40)	AS RSSI_40,
COUNTIF(rssi=-39)	AS RSSI_39,
COUNTIF(rssi=-38)	AS RSSI_38,
COUNTIF(rssi=-37)	AS RSSI_37,
COUNTIF(rssi=-36)	AS RSSI_36,
COUNTIF(rssi=-35)	AS RSSI_35,
COUNTIF(rssi=-34)	AS RSSI_34,
COUNTIF(rssi=-33)	AS RSSI_33,
COUNTIF(rssi=-32)	AS RSSI_32,
COUNTIF(rssi=-31)	AS RSSI_31,
COUNTIF(rssi=-30)	AS RSSI_30,
COUNTIF(rssi=-29)	AS RSSI_29,
COUNTIF(rssi=-28)	AS RSSI_28,
COUNTIF(rssi=-27)	AS RSSI_27,
COUNTIF(rssi=-26)	AS RSSI_26,
COUNTIF(rssi=-25)	AS RSSI_25,
COUNTIF(rssi=-24)	AS RSSI_24,
COUNTIF(rssi=-23)	AS RSSI_23,
COUNTIF(rssi=-22)	AS RSSI_22,
COUNTIF(rssi=-21)	AS RSSI_21,
COUNTIF(rssi=-20)	AS RSSI_20,
COUNTIF(rssi=-19)	AS RSSI_19,
COUNTIF(rssi=-18)	AS RSSI_18,
COUNTIF(rssi=-17)	AS RSSI_17,
COUNTIF(rssi=-16)	AS RSSI_16,
COUNTIF(rssi=-15)	AS RSSI_15,
COUNTIF(rssi=-14)	AS RSSI_14,
COUNTIF(rssi=-13)	AS RSSI_13,
COUNTIF(rssi=-12)	AS RSSI_12,
COUNTIF(rssi=-11)	AS RSSI_11,
COUNTIF(rssi=-10)	AS RSSI_10,
COUNTIF(rssi=-9)	AS RSSI_9,
COUNTIF(rssi=-8)	AS RSSI_8,
COUNTIF(rssi=-7)	AS RSSI_7,
COUNTIF(rssi=-6)	AS RSSI_6,
COUNTIF(rssi=-5)	AS RSSI_5,
COUNTIF(rssi=-4)	AS RSSI_4,
COUNTIF(rssi=-3)	AS RSSI_3,
COUNTIF(rssi=-2)	AS RSSI_2,
COUNTIF(rssi=-1)	AS RSSI_1,
  COUNT(DISTINCT(mac_address)) AS DISTINCT_MAC_ADDRESS,
  CASE
    WHEN base.DAY_OF_WEEK = 1 THEN "休日"
    WHEN base.DAY_OF_WEEK = 7 THEN "休日"
    WHEN ( SELECT COUNT(*) FROM wifi_log.holiday WHERE holiday.date = base.DATETIME ) = 1 THEN "休日"
  ELSE "平日"
  END AS HOLIDAY_FLAG
FROM
  LOG_DATA base
WHERE 
  ((hour <= 8 OR hour >= 22) 
OR (datetime NOT IN (20210403,20210410,20210417) AND hour >= 21) 
OR datetime = 20210406
OR datetime = 20210412 )
IS NOT TRUE
GROUP BY
  DATETIME,
  base.DAY_OF_WEEK,
--   hour,
--   HOUR_INT,
  base.DEVICE_ID
ORDER BY
  DATETIME
--   ,
--   HOUR_INT