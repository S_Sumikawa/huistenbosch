CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_ML_trainingdate_hourly` AS /*要変更点#0*/

SELECT t0.*,
visitor * ratio AS cnt
FROM `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_ML_ratio` t0
INNER JOIN `labs-analytics-298709.works_sumikawa.20210602_huistenbosch_wifi_ML_trainingdate_daily`t1
USING(datetime)
ORDER BY 1,2,3