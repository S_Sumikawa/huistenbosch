DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date DATE;
DECLARE lat1,
lat2,
lon1,
lon2 FLOAT64;
DECLARE 
opening_time1,
closing_time1 INT64;

-- ★★★変数設定★★★
-- ターゲット期間1
SET start_date1 = '2020-01-02';
SET end_date1 = '2020-01-01';

-- ターゲット期間2
SET start_date2 = '2021-04-01';
SET end_date2 = '2021-05-31';

-- 前後比較の区切りの日付(flag=before,afterの区切り)
-- 日付 = separate_dateの場合はbeforeのフラグがつく
SET separate_date = '2020-02-29';

-- ログ検索範囲設定用
SET lat1 = 23;
SET lat2 = 46;
SET lon1 = 123;
SET lon2 = 148;

-- 開園時間、閉園時間
SET opening_time1 = 9;
SET closing_time1 = 22;
-- ★★★設定完了★★★

CREATE OR REPLACE TABLE `labs-analytics-298709.works_sumikawa.20210531_huistenbosch_beacon_log` AS /*要変更点#0*/

WITH BB_beacon AS(
    SELECT DISTINCT *
    FROM `beaconbank-analytics.daily_copy.BB_beacon`
    WHERE uuid = '81D7FDC5-70B7-4475-821D-14E9F2312FCA' AND major = 181 
    AND (minor BETWEEN 1 AND 111)
    AND name LIKE "%huistenbosch%"
) 
-- ,
-- mst_beacon AS( -- 現状不要
--     SELECT
--         BB_beacon.id AS beacon_id,
--         BB_beacon.account_name AS client_name,
--         BB_beacon.name,
--         BB_beacon.uuid,
--         BB_beacon.major,
--         BB_beacon.minor,
--         BB_beacon.valid_from AS start_date,
--         BB_beacon.valid_to AS end_date,
--         BB_beacon_attribute.install_loc_name,
--         BB_beacon_attribute.address_prefecture AS pref,
--         BB_beacon_attribute.address_city AS city,
--         BB_beacon_attribute.address_detail AS address_line1,
--         BB_beacon_attribute.building_name AS address_line2,
--         BB_beacon_attribute.building_floor AS floor
--     FROM
--         `beaconbank-analytics.daily_copy.BB_beacon_attribute` BB_beacon_attribute
--         JOIN BB_beacon ON 
        -- BB_beacon.attr_fact_id= BB_beacon_attribute.id
-- ) ,
-- beacon_log AS(
--     SELECT
--         jst_date,
--         hour,
--         beacon_id,
-- --         install_loc_name,
--         flag,
--         COUNT(DISTINCT id) AS beacon_huu
--     FROM(
        SELECT DISTINCT 
            IF(adid IS NULL, CAST(app_user_id AS STRING), adid) AS id,
            DATE(TIMESTAMP(detected_time), 'Asia/Tokyo') AS jst_date,
            EXTRACT(HOUR FROM DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo')) AS hour,
            DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo') AS jst_time,
            t2.id AS beacon_id,
            t1.latitude,
            t1.longitude,

--             t2.install_loc_name,
            CASE
                WHEN
                    DATE(TIMESTAMP(detected_time), 'Asia/Tokyo') <= separate_date
                THEN "before"
                ELSE "after"
            END AS flag
        FROM
            `beaconbank-analytics.daily_copy.BB_beaconlog*` t1
            JOIN BB_beacon t2 ON t1.beacon_id = t2.id
            -- JOIN mst_beacon t2 ON t1.beacon_id = t2.beacon_id
        WHERE  
            (
                _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date1)
                AND FORMAT_DATE("%Y%m%d", end_date1)
                OR _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date2)
                AND FORMAT_DATE("%Y%m%d", end_date2)
            )
            AND t1.latitude BETWEEN lat1 AND lat2
            AND t1.longitude BETWEEN lon1 AND lon2
            AND t1.app_user_id IS NOT NULL
            AND t1.application_id NOT IN (319830020, 319830042)
    -- )
    -- WHERE
    AND
        EXTRACT(HOUR FROM DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo')) BETWEEN opening_time1 AND closing_time1 --- 時間設定
--     GROUP BY
--         jst_date,
--         hour,
--         beacon_id,
-- --         install_loc_name,
--         flag
-- ) ,
-- beacon_log_all AS(
--     SELECT
--         jst_date,
--         hour,
--         flag,
--         COUNT(DISTINCT id) AS all_beacon_huu
--     FROM(
--         SELECT
--             IF(adid IS NULL, CAST(app_user_id AS STRING), adid) AS id,
--             DATE(TIMESTAMP(detected_time), 'Asia/Tokyo') AS jst_date,
--             EXTRACT(HOUR FROM DATETIME(TIMESTAMP(detected_time), 'Asia/Tokyo')) AS hour,
--             CASE
--                 WHEN
--                     DATE(TIMESTAMP(detected_time), 'Asia/Tokyo') <= separate_date
--                 THEN "before"
--                 ELSE "after"
--             END AS flag
--         FROM
--             `beaconbank-analytics.daily_copy.BB_beaconlog*` t1
--             JOIN BB_beacon t2 ON t1.beacon_id = t2.id
--             -- JOIN mst_beacon t2 ON t1.beacon_id = t2.beacon_id
--         WHERE  
--             (
--                 _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date1)
--                 AND FORMAT_DATE("%Y%m%d", end_date1)
--                 OR _TABLE_SUFFIX BETWEEN FORMAT_DATE("%Y%m%d", start_date2)
--                 AND FORMAT_DATE("%Y%m%d", end_date2)
--             )
--             AND t1.latitude BETWEEN lat1 AND lat2
--             AND t1.longitude BETWEEN lon1 AND lon2
--             AND t1.app_user_id IS NOT NULL
--             AND t1.application_id NOT IN (319830020, 319830042)
--     )
--     WHERE
--         hour BETWEEN opening_time1 AND closing_time1 --- 時間設定
--     GROUP BY
--         jst_date,
--         hour,
--         flag
-- )
-- SELECT
--     t1.*,
--     t2.all_beacon_huu
-- FROM
--     beacon_log t1
--     LEFT JOIN beacon_log_all t2 USING (jst_date,hour)
-- ORDER BY 
--     jst_date
--     ,hour