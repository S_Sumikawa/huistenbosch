/* 分析_placelistを用いたログの抽出 */
-- ☆☆☆以下は設定なので書き換えないこと☆☆☆

-- ★★★設定終了★★★
CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210607_huistenbosch_travel_poi` AS /*要変更点#0*/

WITH place AS (
  SELECT
    place_id,
    place_name,
    chain_name,
    cat_tmp,
    cat_lv1_jp,
    cat_lv2_jp,
    cat_lv3_jp,
    pl_lat,
    pl_lon,
    radius,
    prefecture,
    (pl_lat - (500 / 30.8184 * 0.000277778)) AS lat_min,
    (pl_lat + (500 / 30.8184 * 0.000277778)) AS lat_max,
    (pl_lon - (500 / 25.2450 * 0.000277778)) AS lon_min,
    (pl_lon + (500 / 25.2450 * 0.000277778)) AS lon_max
  FROM
    `beaconbank-analytics.huistenbosch.20210604_huistenbosch_target_places`
  GROUP BY
    place_id,
    place_name,
    chain_name,
    cat_tmp,
    cat_lv1_jp,
    cat_lv2_jp,
    cat_lv3_jp,
    pl_lat,
    pl_lon,
    radius,
    prefecture
) 
, logs AS(
    SELECT 
        *
    FROM 
        `beaconbank-analytics.huistenbosch.20210607_huistenbosch_travel_behavior`
    WHERE
        trip_flag = 1
)

SELECT
  DISTINCT t0.id AS id,
  jst_date,
  place_name,
  flag,
  visit_flag,
  cnt_date,
  night_before,
  night_after,
  recent_visit,
  next_visit,
  Gender,
  Age,
  home_pref,
  home_city,
  office_pref,
  office_city
FROM
  (
    /* 説明)業態によって推奨抽出半径が異なる。抽出半径ごとに処理することで処理スピードが上がる */
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),25)
      AND radius = 25
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),30)
      AND radius = 30
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),50)
      AND radius = 50
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),80)
      AND radius = 80
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),100)
      AND radius = 100
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),150)
      AND radius = 150
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),200)
      AND radius = 200
    UNION
    DISTINCT
    SELECT
      *
    FROM
      place
      JOIN logs ON logs.latitude BETWEEN place.lat_min AND place.lat_max
      AND logs.longitude BETWEEN place.lon_min AND place.lon_max
    WHERE
      prefecture != home_pref
      AND ST_DWITHIN(ST_GeogPoint(pl_lon, pl_lat),ST_GeogPoint(longitude, latitude),250)
      AND radius = 250
  ) t0