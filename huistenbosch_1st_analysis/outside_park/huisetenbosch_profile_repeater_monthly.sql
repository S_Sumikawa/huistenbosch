CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210610_huistenbosch_profile_repeater_monthly_test` AS /*要変更点#0*/

WITH t0 AS(
SELECT
    id,
    cnt_date,
    visit_per_month,
    jst_date,
    flag,
    CASE
        WHEN flag = 'after' AND jst_date < '2020-12-01' THEN 1
        WHEN flag = 'after' AND jst_date >= '2020-12-01' THEN 2
    END AS after_term,
    CASE 
        WHEN repeat_flag = 'repeat' THEN 1
        ELSE   2
    END AS repeat_flag_num,
    home_flag,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
FROM 
    `beaconbank-analytics.huistenbosch.20210607_huistenbosch_profile_repeater`
)
, t1 AS(
    SELECT
    id,
    cnt_date,
    visit_per_month,
    -- DATE_TRUNC(jst_date, MONTH) AS jst_ym,
    flag,
    after_term,
    home_flag,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city,
    CASE
        WHEN MIN(repeat_flag_num) = 1 THEN 'repeat'
        ELSE 'new'
    END AS repeat_flag_n,
    COUNT(DISTINCT jst_date) AS monthly_visit
 FROM
    t0
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
)
SELECT * FROM t1