/*分析 行動DNAの付与(grm:グルメ)*/
--ショップ・男女別のスコアの合計と人数を算出
--wiki url:
-- その他変更点(#numberでコード内検索)
    -- #0 出力する一時テーブル名
    -- #1 来訪者情報のテーブル名

create or replace table `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_grm_tmp` AS /*要変更点#0*/  

WITH log AS(
  SELECT
    t1.*,
    CASE
      WHEN t2.Gender = 'M' THEN '男性'
      WHEN t2.Gender = 'F' THEN '女性'
      ELSE NULL
      END gender
  FROM(
    SELECT DISTINCT
      flag, home_flag,
      t1.*
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT 
            *
        FROM 
            `beaconbank-analytics.huistenbosch.20210608_huistenbosch_all_profile` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
  ) t1
  LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_gender` t2 USING(id)
) ,
unpivot_process_grm AS(
  -- GRM_CFS
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_CFS_Cafe' AS category, GRM_CFS_Cafe AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_CFS_JapaneseSweets' AS category, GRM_CFS_JapaneseSweets AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_CFS_OtherSweets' AS category, GRM_CFS_OtherSweets AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_CFS_WesternSweets' AS category, GRM_CFS_WesternSweets AS score FROM log
  
  -- GRM_FMR
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FMR_Chinese' AS category, GRM_FMR_Chinese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FMR_Japanese' AS category, GRM_FMR_Japanese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FMR_Sushi' AS category, GRM_FMR_Sushi AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FMR_Variety' AS category, GRM_FMR_Variety AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FMR_Western' AS category, GRM_FMR_Western AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FMR_Yakiniku' AS category, GRM_FMR_Yakiniku AS score FROM log
  
  -- GRM_FST
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_Bakery' AS category, GRM_FST_Bakery AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_Curry' AS category, GRM_FST_Curry AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_DonTeishoku' AS category, GRM_FST_DonTeishoku AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_HumbergerShop' AS category, GRM_FST_HumbergerShop AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_OtherFastFood' AS category, GRM_FST_OtherFastFood AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_Ramen' AS category, GRM_FST_Ramen AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_Takeout' AS category, GRM_FST_Takeout AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_FST_UdonSoba' AS category, GRM_FST_UdonSoba AS score FROM log
  
  -- GRM_OTR
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Asian' AS category, GRM_OTR_Asian AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Bar' AS category, GRM_OTR_Bar AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Chinese' AS category, GRM_OTR_Chinese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Ethnic' AS category, GRM_OTR_Ethnic AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_French' AS category, GRM_OTR_French AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Italian' AS category, GRM_OTR_Italian AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Izakaya' AS category, GRM_OTR_Izakaya AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_NabeOden' AS category, GRM_OTR_NabeOden AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_NigiriSushi' AS category, GRM_OTR_NigiriSushi AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Okonomiyaki' AS category, GRM_OTR_Okonomiyaki AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_OtherRestaurant' AS category, GRM_OTR_OtherRestaurant AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_PubClub' AS category, GRM_OTR_PubClub AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Steak' AS category, GRM_OTR_Steak AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Tonkatsu' AS category, GRM_OTR_Tonkatsu AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_TraditionalJapanese' AS category, GRM_OTR_TraditionalJapanese AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Unagi' AS category, GRM_OTR_Unagi AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Yakiniku' AS category, GRM_OTR_Yakiniku AS score FROM log
  UNION ALL
  SELECT id, flag, home_flag, gender,area,place_name, 'GRM_OTR_Yakitori' AS category, GRM_OTR_Yakitori AS score FROM log
)

SELECT
  flag, home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  SUM(score) AS sum_score,
  COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
GROUP BY
  flag, home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp