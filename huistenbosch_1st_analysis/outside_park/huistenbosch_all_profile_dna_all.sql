/*分析 行動DNAの付与_集計*/
--一時テーブルの値を集計して、ショップ別の行動DNA、ショップ・男女別の行動DNAを算出
--wiki url:
-- その他変更点(#numberでコード内検索)
    -- #0 出力するテーブル名
    -- #1 DNA情報の一時テーブル名

create or replace table `beaconbank-analytics.huistenbosch.20210616_huistenbosch_all_profile_dna_all` AS /*要変更点#0*/

WITH dna_etm_male AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS male_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_etm_tmp` /*要変更点#1*/
  WHERE
    gender = '男性'
),
dna_etm_female AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS female_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_etm_tmp` /*要変更点#1*/
  WHERE
    gender = '女性'
),
dna_etm_total AS( /*行動DNA*/
  SELECT
    flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score_total/uu_total AS ave_score
  FROM
    ( /*男女のscoreとuuを足し合わせる*/
      SELECT
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp,
        sum(sum_score) AS sum_score_total,
        sum(uu) AS uu_total
      FROM
        `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_etm_tmp` /*要変更点#1*/
      GROUP BY
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp
    )

)
, dne_etm AS(
SELECT
  dna_etm_total.*,
  male_ave_score,
  female_ave_score
FROM
  dna_etm_total
  LEFT JOIN dna_etm_male
    USING(flag,Cat_Lv3_jp)
  LEFT JOIN dna_etm_female
    USING(flag,Cat_Lv3_jp)
)


,dna_grm_male AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS male_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_grm_tmp` /*要変更点#1*/
  WHERE
    gender = '男性'
),
dna_grm_female AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS female_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_grm_tmp` /*要変更点#1*/
  WHERE
    gender = '女性'
),
dna_grm_total AS( /*行動DNA*/
  SELECT
    flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score_total/uu_total AS ave_score
  FROM
    ( /*男女のscoreとuuを足し合わせる*/
      SELECT
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp,
        sum(sum_score) AS sum_score_total,
        sum(uu) AS uu_total
      FROM
        `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_grm_tmp` /*要変更点#1*/
      GROUP BY
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp
    )

)
, dne_grm AS(
SELECT
  dna_grm_total.*,
  male_ave_score,
  female_ave_score
FROM
  dna_grm_total
  LEFT JOIN dna_grm_male
    USING(flag,Cat_Lv3_jp)
  LEFT JOIN dna_grm_female
    USING(flag,Cat_Lv3_jp)
)

,dna_lfs_male AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS male_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_lfs_tmp` /*要変更点#1*/
  WHERE
    gender = '男性'
),
dna_lfs_female AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS female_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_lfs_tmp` /*要変更点#1*/
  WHERE
    gender = '女性'
),
dna_lfs_total AS( /*行動DNA*/
  SELECT
    flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score_total/uu_total AS ave_score
  FROM
    ( /*男女のscoreとuuを足し合わせる*/
      SELECT
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp,
        sum(sum_score) AS sum_score_total,
        sum(uu) AS uu_total
      FROM
        `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_lfs_tmp` /*要変更点#1*/
      GROUP BY
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp
    )

)
, dne_lfs AS(
SELECT
  dna_lfs_total.*,
  male_ave_score,
  female_ave_score
FROM
  dna_lfs_total
  LEFT JOIN dna_lfs_male
    USING(flag,Cat_Lv3_jp)
  LEFT JOIN dna_lfs_female
    USING(flag,Cat_Lv3_jp)
)

,dna_spg_male AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS male_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_spg_tmp` /*要変更点#1*/
  WHERE
    gender = '男性'
),
dna_spg_female AS( /*男女別の行動DNA*/
  SELECT
    flag,
    category,
    gender,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score/uu AS female_ave_score
  FROM
    `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_spg_tmp` /*要変更点#1*/
  WHERE
    gender = '女性'
),
dna_spg_total AS( /*行動DNA*/
  SELECT
    flag,
    category,
    Cat_Lv1_jp,
    Cat_Lv2_jp,
    Cat_Lv3_jp,
    sum_score_total/uu_total AS ave_score
  FROM
    ( /*男女のscoreとuuを足し合わせる*/
      SELECT
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp,
        sum(sum_score) AS sum_score_total,
        sum(uu) AS uu_total
      FROM
        `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_spg_tmp` /*要変更点#1*/
      GROUP BY
        flag,
        category,
        Cat_Lv1_jp,
        Cat_Lv2_jp,
        Cat_Lv3_jp
    )

)
, dne_spg AS(
SELECT
  dna_spg_total.*,
  male_ave_score,
  female_ave_score
FROM
  dna_spg_total
  LEFT JOIN dna_spg_male
    USING(flag,Cat_Lv3_jp)
  LEFT JOIN dna_spg_female
    USING(flag,Cat_Lv3_jp)
)

SELECT * FROM dne_etm
UNION ALL SELECT * FROM dne_grm
UNION ALL SELECT * FROM dne_lfs
UNION ALL SELECT * FROM dne_spg