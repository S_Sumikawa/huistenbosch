/*宿泊行動 */
-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
DECLARE 
nh_start,
nh_end INT64; 
DECLARE target_name1 STRING;
DECLARE target_poly1 STRING;

-- ★★★設定項目★★★

-- 夜間の時間帯(ex.23~5)
-- nh = night_hour
SET nh_start = 23;
SET nh_end = 5;

-- 追加で宿泊判定をする対象施設ポリゴン
SET target_poly1 = 'POLYGON((129.78205143090764 33.07835732316272,129.78219090577642 33.07812358391031,129.78372512933294 33.0782629285394,129.78587626096288 33.07994094642577,129.78602110024968 33.08198610156933,129.78632687207738 33.08196812239075,129.78637515183965 33.08225578880849,129.7859398408834 33.082355053138926,129.78600113514284 33.08309912492111,129.78626627728738 33.083227414448146,129.78640953038254 33.08304388872046,129.78917976212063 33.08287011053718,129.78980105426348 33.08414065430174,129.79152107175383 33.08433100348544,129.79110945458916 33.08502632589806,129.7910586964561 33.08620121471334,129.7913380538701 33.087036299631215,129.79091508019866 33.08714236290895,129.79166772942864 33.08871182359587,129.79188661906466 33.08888124879444,129.7924702891267 33.08883494061111,129.7925948291466 33.08998005584629,129.78986665717986 33.090216336850176,129.7891685520388 33.090225977745845,129.7890900371802 33.090963703982716,129.78769187548625 33.09082952596329,129.78692805622433 33.09023917012475,129.78641100019178 33.08956791103889,129.78771577438766 33.08878269066757,129.78662631556818 33.087610355058665,129.78576168034652 33.08756685566208,129.7858348543612 33.08664836788825,129.7855486123679 33.08584448281953,129.78529455688277 33.08520239722506,129.7836560624508 33.08432402190212,129.7840966152815 33.08369092888635,129.7829821574358 33.08192131330081,129.78253959294835 33.08009005051815,129.7823303806452 33.07992900685026,129.78205143090764 33.07835732316272))'
; -- 現在はハウステンボス


-- その他変更点(#numberでコード内検索)
-- #0 出力する一次テーブル名

-- ★★★設定終了★★★
CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210610_huistenbosch_travel_behavior_wtout_mode_v2` AS /*要変更点#0*/
WITH travel_behavior AS( 
    SELECT 
        sb.id,
        sb.jst_time,
        sb.jst_date,
        sb.hour,
        sb.jst_ym,
        sb.visit_date,
        sb.longitude,
        sb.latitude,
    -- store_name,
    -- log_type,
        sb.flag,
        sb.visit_flag,
        sb.repeat_flag,
        sb.cnt_date,
        sb.visit_per_month,
        sb.home_flag,
        pt.night_before,
        pt.night_after,
        pt.recent_visit,
        pt.next_visit,
        pt.Gender,
        pt.Age,
        pt.home_lat,
        pt.home_lon,
        pt.home_pref,
        pt.home_city,
        pt.office_pref,
        pt.office_city,
        -- tr.mode
    FROM 
        `beaconbank-analytics.huistenbosch.20210607_huistenbosch_sameday_behavior_v6` sb
        INNER JOIN `beaconbank-analytics.huistenbosch.20210607_huistenbosch_profile_travel` pt USING(id,visit_date)
        -- INNER JOIN `beaconbank-analytics.huistenbosch.20210607_huistenbosch_06_Trans_Ratio` tr USING(id,visit_date)
    WHERE
        (
            (visit_flag = '1day before'
            AND recent_visit = jst_date)
            OR
            (visit_flag = '1day after'
            AND next_visit = jst_date)
        ) IS NOT TRUE
)
SELECT
    tb.*,
    CASE --旅中フラグ：１=旅中行動判定
        WHEN night_before = 2 -- その他九州で前泊 かつ
            AND　(visit_flag = '1day before' OR (visit_flag = 'visit day' AND tb.hour <= nh_end)) -- 前日のログ または 当日朝のログ
            THEN 1
        WHEN night_after = 2 -- その他九州で当日泊 かつ
            AND (visit_flag = '1day after'  OR (visit_flag = 'visit day' AND tb.hour >= nh_start)) -- 翌日のログ または 当日夜のログ
            THEN 1
        WHEN  night_before = 1 -- HTBで前泊　かつ
            AND  recent_visit != visit_date - 1 -- 前日にHTB来訪ログなし　かつ
            AND　visit_flag = '1day before' -- 前日のログ
            THEN 1
        WHEN  night_after = 1 -- HTBで当日泊　かつ
            AND next_visit != visit_date + 1  -- 翌日にHTB来訪ログなし　かつ
            AND　visit_flag = '1day after' -- 翌日のログ
            THEN 1
        ELSE 0
    END AS trip_flag,
FROM
    travel_behavior tb