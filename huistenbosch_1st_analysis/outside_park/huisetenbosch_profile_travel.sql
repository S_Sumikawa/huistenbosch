/*分析 宿泊判定 */
-- BeaconBankとVerasetのGPSのログを使用
-- 分析対象店舗のポリゴンを入力してログを取得
-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
DECLARE start_date1,
end_date1,
start_date2,
end_date2,
separate_date DATE;

DECLARE 
 min_visit_cnt,
 max_visit_cnt INT64;

DECLARE lat1,
lat2,
lon1,
lon2 INT64;

DECLARE target_lat1,
target_lon1 FLOAT64;
DECLARE radius,
radius2,
nh_start,
nh_end INT64; 
DECLARE target_name1 STRING;
DECLARE target_poly1 STRING;

-- ★★★設定項目★★★

-- 帰宅判定をする半径
SET radius = 500;

-- 夜間の時間帯(ex.23~5)
-- nh = night_hour
SET nh_start = 23;
SET nh_end = 5;

-- 追加で宿泊判定をする対象施設ポリゴン
SET target_poly1 = 'POLYGON((129.78205143090764 33.07835732316272,129.78219090577642 33.07812358391031,129.78372512933294 33.0782629285394,129.78587626096288 33.07994094642577,129.78602110024968 33.08198610156933,129.78632687207738 33.08196812239075,129.78637515183965 33.08225578880849,129.7859398408834 33.082355053138926,129.78600113514284 33.08309912492111,129.78626627728738 33.083227414448146,129.78640953038254 33.08304388872046,129.78917976212063 33.08287011053718,129.78980105426348 33.08414065430174,129.79152107175383 33.08433100348544,129.79110945458916 33.08502632589806,129.7910586964561 33.08620121471334,129.7913380538701 33.087036299631215,129.79091508019866 33.08714236290895,129.79166772942864 33.08871182359587,129.79188661906466 33.08888124879444,129.7924702891267 33.08883494061111,129.7925948291466 33.08998005584629,129.78986665717986 33.090216336850176,129.7891685520388 33.090225977745845,129.7890900371802 33.090963703982716,129.78769187548625 33.09082952596329,129.78692805622433 33.09023917012475,129.78641100019178 33.08956791103889,129.78771577438766 33.08878269066757,129.78662631556818 33.087610355058665,129.78576168034652 33.08756685566208,129.7858348543612 33.08664836788825,129.7855486123679 33.08584448281953,129.78529455688277 33.08520239722506,129.7836560624508 33.08432402190212,129.7840966152815 33.08369092888635,129.7829821574358 33.08192131330081,129.78253959294835 33.08009005051815,129.7823303806452 33.07992900685026,129.78205143090764 33.07835732316272))'
; -- 現在はハウステンボス

-- 追加で宿泊判定をする対象施設の宿泊判定範囲
-- ex. 上記のポリゴンから半径500以内に夜間にログがあれば、その施設内に宿泊したと判定する(night_before/afterのフラグが1になる)
SET radius2 = 600;


-- その他変更点(#numberでコード内検索)
-- #0 出力する一次テーブル名

-- ★★★設定終了★★★

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210607_huistenbosch_profile_travel` AS /*要変更点#0*/

WITH stay_judgement_log1 AS( -- 帰宅判定
    SELECT
        id,
        CASE
            WHEN visit_flag = '1day before' THEN DATE_ADD(jst_date, INTERVAL 1 day)
            WHEN visit_flag = '1day after' THEN DATE_SUB(jst_date, INTERVAL 1 day)
            ELSE jst_date
        END AS  visit_date,
        longitude,
        latitude,
        visit_flag,
        flag,
        repeat_flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,

        -- night_before:前泊判定
        -- night_after:後泊判定
        --      0:非宿泊（帰宅）
        --      1:追加した施設内に宿泊（ハウステンボス）
        --      2:宿泊
        --      null:夜間ログなし

        CASE
            WHEN -- 居住地周辺のログの場合
                (   (hour <= nh_end AND visit_flag IN ('visit day')) 
                    OR (hour >= nh_start AND visit_flag IN ('1day before'))
                    )
                AND ST_DWITHIN(ST_GeogPoint(home_lon, home_lat),ST_GeogPoint(longitude,latitude),radius)
                THEN 0
            WHEN 
                (   (hour <= nh_end AND visit_flag IN ('visit day')) 
                    OR (hour >= nh_start AND visit_flag IN ('1day before'))
                    )
                THEN 2
        END AS night_before,
        CASE
            WHEN -- 居住地周辺のログの場合
                (   (hour <= nh_end AND visit_flag IN ('1day after')) 
                    OR (hour >= nh_start AND visit_flag IN ('visit day'))
                    )
                AND ST_DWITHIN(ST_GeogPoint(home_lon, home_lat),ST_GeogPoint(longitude,latitude),radius)
                THEN 0
            WHEN 
                (   (hour <= nh_end AND visit_flag IN ('1day after')) 
                    OR (hour >= nh_start AND visit_flag IN ('visit day'))
                    )
                THEN 2
        END AS night_after
    FROM
        `beaconbank-analytics.huistenbosch.20210607_huistenbosch_sameday_behavior_v6`
    WHERE
        (hour <= nh_end 
            AND visit_flag IN ('visit day','1day after')
        )
        OR 
        (hour >= nh_start 
            AND visit_flag IN ('1day before','visit day')
        )
),
stay_judgement_log2 AS( -- HTB内宿泊判定
    SELECT
        id,
        visit_date,
        visit_flag,
        flag,
        repeat_flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        CASE
            WHEN -- ハウステンボス周辺のログの場合
                night_before = 2
                AND ST_DWITHIN(ST_GEOGFROMTEXT(target_poly1),ST_GeogPoint(longitude,latitude),radius2)
                THEN 1
            ELSE night_before
        END AS night_before,
        CASE
            WHEN -- ハウステンボス周辺のログの場合
                night_after = 2
                AND ST_DWITHIN(ST_GEOGFROMTEXT(target_poly1),ST_GeogPoint(longitude,latitude),radius2)
                THEN 1
            ELSE night_after
        END AS night_after
    FROM
        stay_judgement_log1
),
stay_judgement_id AS( -- 個々人について宿泊判定
    SELECT
        id,
        visit_date,
        flag,
        repeat_flag,
        Gender,
        Age,
        home_lat,
        home_lon,
        home_pref,
        home_city,
        office_pref,
        office_city,
        MIN(night_before) AS night_before,
        MIN(night_after) AS night_after,
    FROM
        stay_judgement_log2 AS log
    GROUP BY
        1,2,3,4,5,6,7,8,9,10,11,12
)
SELECT
    *,
    LAG(visit_date) OVER (PARTITION BY id ORDER BY visit_date) AS recent_visit,
    LEAD(visit_date) OVER (PARTITION BY id ORDER BY visit_date) AS next_visit,
FROM
    stay_judgement_id