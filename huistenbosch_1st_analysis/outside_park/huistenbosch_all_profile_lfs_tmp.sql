/*分析 行動DNAの付与(lfs:生活サービス)*/
--ショップ・男女別のスコアの合計と人数を算出
--wiki url:
-- その他変更点(#numberでコード内検索)
    -- #0 出力する一時テーブル名
    -- #1 来訪者情報のテーブル名

create or replace table `beaconbank-analytics.huistenbosch.20210609_huistenbosch_all_profile_lfs_tmp` AS /*要変更点#0*/  

WITH log AS(
  SELECT
    t1.*,
    CASE
      WHEN t2.Gender = 'M' THEN '男性'
      WHEN t2.Gender = 'F' THEN '女性'
      ELSE NULL
      END gender
  FROM(
    SELECT DISTINCT
      flag, home_flag,
      t1.*
    FROM
      `beaconbank-analytics.user_profile_v2.vw_dna_lv3_18m` t1
      JOIN 
        (SELECT 
            *
        FROM 
            `beaconbank-analytics.huistenbosch.20210608_huistenbosch_all_profile` /*要変更点#1*/
        WHERE
            visit_per_month <= 15
        ) t2
      USING(id)
  ) t1
  LEFT JOIN `beaconbank-analytics.user_profile_v2.vw_gender` t2 USING(id)
) ,
unpivot_process AS(
  -- LFS_EDU
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_ElementarySchool' AS category, LFS_EDU_ElementarySchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_HighSchool' AS category, LFS_EDU_HighSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_Hndicapped' AS category, LFS_EDU_Hndicapped AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_JuniorCollege' AS category, LFS_EDU_JuniorCollege AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_JuniorHighSchool' AS category, LFS_EDU_JuniorHighSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_KindergartenNursery' AS category, LFS_EDU_KindergartenNursery AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_PrepSchool' AS category, LFS_EDU_PrepSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_TechnicalCollege' AS category, LFS_EDU_TechnicalCollege AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_Univercity' AS category, LFS_EDU_Univercity AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_EDU_VocationalSchool' AS category, LFS_EDU_VocationalSchool AS score FROM log

  -- LFS_FIN, LFS_HSP は使わない

  -- LFS_EDU
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_OTR_BeautySalons' AS category, LFS_OTR_BeautySalons AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_OTR_Laundry' AS category, LFS_OTR_Laundry AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_OTR_Massage' AS category, LFS_OTR_Massage AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_OTR_PetService' AS category, LFS_OTR_PetService AS score FROM log

  -- LFS_PUB は使わない

  -- LFS_SCL
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_AbacusShool' AS category, LFS_SCL_AbacusShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_BalletShool' AS category, LFS_SCL_BalletShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_BusinessSchool' AS category, LFS_SCL_BusinessSchool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_CalligraphyShool' AS category, LFS_SCL_CalligraphyShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_CeramicArtShool' AS category, LFS_SCL_CeramicArtShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_ComputerShool' AS category, LFS_SCL_ComputerShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_CookingShool' AS category, LFS_SCL_CookingShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_DanceShool' AS category, LFS_SCL_DanceShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_DressingShool' AS category, LFS_SCL_DressingShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_English' AS category, LFS_SCL_English AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_FlowerArrangementShool' AS category, LFS_SCL_FlowerArrangementShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_ForeignLang' AS category, LFS_SCL_ForeignLang AS score FROM log
   UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_GlassCraftShool' AS category, LFS_SCL_GlassCraftShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_KnittingShool' AS category, LFS_SCL_KnittingShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_MusicShool' AS category, LFS_SCL_MusicShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_PaintingArtShool' AS category, LFS_SCL_PaintingArtShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_PianoShool' AS category, LFS_SCL_PianoShool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_SCL_TeaCeremonyShool' AS category, LFS_SCL_TeaCeremonyShool AS score FROM log

  -- LFS_TRS （LFS_TRS_CarCareService, LFS_TRS_FerryTerminal は除外する）
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_Airport' AS category, LFS_TRS_Airport AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_BusTerminal' AS category, LFS_TRS_BusTerminal AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_Carpool' AS category, LFS_TRS_Carpool AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_GasStation' AS category, LFS_TRS_GasStation AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_RentaCar' AS category, LFS_TRS_RentaCar AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_RoadsideRest' AS category, LFS_TRS_RoadsideRest AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_SAPA' AS category, LFS_TRS_SAPA AS score FROM log
  UNION ALL
  SELECT id,  flag, home_flag, gender,area,place_name, 'LFS_TRS_Station' AS category, LFS_TRS_Station AS score FROM log
)

SELECT
  flag, home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp,
  SUM(score) AS sum_score,
  COUNT(DISTINCT id) AS uu
FROM
  unpivot_process t1
  LEFT JOIN `labs-analytics-298709.works_hirota.00_mst_DNA_jp_en_v2` t2 ON t1.category = t2.genre_Lv3_en
WHERE
  score IS NOT NULL
GROUP BY
  flag, home_flag,
  gender,
  category,
  Cat_Lv1_jp,
  Cat_Lv2_jp,
  Cat_Lv3_jp