-- CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20210610_huistenbosch_profile_repeater_frequency` AS
SELECT
    id,
    visit_per_month AS visit_per_month_bf,
    first,
    second,
    (first + second) /2 AS visit_per_month,
    cnt_date,
    flag,
    after_term,
    home_flag,
    Gender,
    Age,
    home_lat,
    home_lon,
    home_pref,
    home_city,
    office_pref,
    office_city
FROM
  `beaconbank-analytics.huistenbosch.20210610_huistenbosch_profile_repeater_monthly`
WHERE}
    flag = 'after'
PIVOT( SUM(monthly_visit) FOR after_term IN (1 AS first,
  2 AS second))
ORDER BY
  id, flag