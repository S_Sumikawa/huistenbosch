-- version_202110
/*分析 来訪日ファイル作成 ポリゴン*/
-- ☆☆☆以下は説明なので書き換えないこと☆☆☆
  DECLARE start_date,end_date DATE;
  DECLARE lat1,lat2,lon1,lon2 INT64;
  DECLARE filter_geo, data_source ARRAY<STRING>;
  DECLARE geo_length INT64;
  DECLARE target_name1,target_name2,target_poly1,target_poly2 STRING;
-- ログ検索範囲設定用(原則書き換えない)
  SET lat1 = 23;
  SET lat2 = 46;
  SET lon1 = 123;
  SET lon2 = 148;

-- ★★★変数設定★★★
  -- ターゲット期間
      SET start_date = '2021-04-01';
      SET end_date = '2021-11-3';

  -- 対象店舗/エリア1
      -- ポリゴン
      SET target_poly1 = 'POLYGON((129.78745868358436 33.08512949716434,129.78723874244514 33.08500364733457,129.78691151294532 33.084724979213455,129.78687932643714 33.08462609676469,129.78706171665016 33.08439686884218,129.78773226890388 33.08477442039615,129.78745868358436 33.08512949716434))'
      ;
      -- 店名
      SET target_name1 = 'パサージュ';

  -- 対象店舗/エリア2
  -- ※1店舗の分析をする場合は対象店舗1と同じ情報を入力する
      -- ポリゴン
      -- SET target_poly2 = 'POLYGON((139.759687 35.674909,139.762567 35.67305,139.763768 35.673399,139.765109 35.675342,139.764369 35.67549,139.762395 35.67413,139.762272 35.674026,139.762814 35.675882,139.76365 35.675682,139.764509 35.676405,139.765174 35.677381,139.76591 35.679528,139.765374 35.680242,139.766217 35.682579,139.767162 35.683085,139.768848 35.687287,139.767861 35.68834,139.764256 35.689107,139.761016 35.689403,139.761509 35.687224,139.760243 35.684628,139.759814 35.68315,139.75578 35.676911,139.759687 35.674909))'
      -- ;
      -- -- 店名
      -- SET target_name2 = '店名2';

  -- データソースの指定
      -- bb_beacon:   BeaconBankのビーコンログ
      -- bb_location: BeaconBankのGPSログ
      -- pd:          predicioのGPSログ
      -- vs:          verasetのGPSログ
      -- bw_radiko:   radikoのGPSログ
    SET data_source = ['bb_location','vs'];
  
  -- ジオハッシュの指定（分析対象範囲の設定）
        -- 大きなコストダウンになるので必ず指定してください
    SET filter_geo = ['wvuk1r','wvuk1q'];  -- http://geohash.gofreerange.com/ から分析したいエリア周辺のgeohashの初めのn文字を入れる
    SET geo_length = 6;  -- 上記のgeohashに入れた文字数nを入れる


  -- その他変更点(#numberでコード内検索)
    -- #0 出力する来訪日テーブル名
-- ★★★設定終了★★★

CREATE OR REPLACE TABLE `beaconbank-analytics.huistenbosch.20211108_passage_visitor` AS /*要変更点#0*/
-- INSERT INTO `beaconbank-analytics.huistenbosch.20211108_passage_visitor`

WITH union_logs AS (
  SELECT
    IF(adid IS NULL, CAST(app_user_id AS STRING),adid) AS id,
    date_time AS jst_date_time,
    EXTRACT(HOUR FROM date_time) AS jst_hour,
    EXTRACT(DATE FROM date_time) AS jst_date,
    latitude,
    longitude,
    'gps' AS log_type,
  FROM
    `beaconbank-analytics.analytics_log.gps_*`
  WHERE
     _TABLE_SUFFIX IN UNNEST(data_source)
    AND DATE(date_time) BETWEEN start_date AND end_date
    AND LEFT(geohash,geo_length) IN UNNEST(filter_geo)
    AND application_id NOT IN (11660007)
)

SELECT DISTINCT 
    id,
    jst_date_time,
    jst_date,
    jst_hour,
    log_type,
  CASE
    WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly1), ST_GEOGPOINT(union_logs.longitude, union_logs.latitude))
        THEN target_name1
    -- WHEN ST_COVERS(ST_GEOGFROMTEXT(target_poly2), ST_GEOGPOINT(union_logs.longitude, union_logs.latitude))
    --     THEN target_name2
  END AS place_name,
    bqfunc.holidays_in_japan.NETWORKDAYS_ON_OFF_WEEKEND_DAYS_AND_HOLIDAYS_IN_JAPAN(jst_date, jst_date) AS weekday_flag
    -- weekday_flag : =1 平日
    --                   : =0 土日・祝日 
FROM
    union_logs
WHERE
    (
        ST_COVERS(ST_GEOGFROMTEXT(target_poly1), ST_GEOGPOINT(union_logs.longitude, union_logs.latitude))
        -- OR ST_COVERS(ST_GEOGFROMTEXT(target_poly2), ST_GEOGPOINT(union_logs.longitude, union_logs.latitude))
    )
    AND jst_hour BETWEEN 9 AND 20
 ORDER BY
    2,3